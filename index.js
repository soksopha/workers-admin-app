import { AppRegistry } from 'react-native';
import App from './App';
import './i18n';
import { name as appName } from './app.json';
import 'react-native-reanimated';

AppRegistry.registerComponent(appName, () => App);
