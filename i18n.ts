import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import en from 'src/languages/en.json';
import km from 'src/languages/km.json';

const resources = {
  en: {
    translation: en,
  },
  km: {
    translation: km,
  },
};

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  resources,
  lng: 'en',
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
  react: {
    useSuspense: false,
  },
});

export default i18n;
