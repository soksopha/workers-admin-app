import { configureFonts, MD2LightTheme } from "react-native-paper";
import { Theme } from "./theme";

const fontConfig = {
  ios: {
    regular: {
      fontFamily: "Kantumruy-Regular",
      fontWeight: "400",
      fontSize: 12,
    },
    medium: {
      fontFamily: "Kantumruy-Bold",
      fontWeight: "500",
      fontSize: 12,
    },
    light: {
      fontFamily: "Kantumruy-Regular",
      fontWeight: "300",
      fontSize: 12,
    },
    thin: {
      fontFamily: "Kantumruy-Regular",
      fontWeight: "100",
      fontSize: 12,
    },
    titleMedium: {
      fontFamily: "Kantumruy-Bold",
      fontWeight: "500",
      fontSize: 21,
    },
  },
  default: {
    regular: {
      fontFamily: "Kantumruy-Regular",
      fontWeight: "normal",
      fontSize: 12,
    },
    medium: {
      fontFamily: "Kantumruy-Bold",
      fontWeight: "normal",
      fontSize: 12,
    },
    light: {
      fontFamily: "Kantumruy-Regular",
      fontWeight: "normal",
      fontSize: 12,
    },
    thin: {
      fontFamily: "Kantumruy-Regular",
      fontWeight: "normal",
      fontSize: 12,
    },
  },
};

const theme = {
  ...MD2LightTheme,
  dark: false,
  roundness: 10,
  version: 2,
  colors: {
    ...MD2LightTheme.colors,
    primary: Theme.Colors.primary,
    accent: "#03dac4",
    background: "#f6f6f6",
    surface: "white",
    error: "#B00020",
  },
  fonts: configureFonts({ config: fontConfig, isV3: false }),
  style: {
    ...MD2LightTheme.style,
    button: {
      height: 50,
    },
  },
};

export default theme;
