const Colors = {
  accent: "#002F91",
  primary: "#62D26D",
  success: "#28ABA0",
  active: "#FFF0BF",
  warning: "#FFF0BF",
  danger: "#87031D",
  link: "#009688",
  border: "#ECECEC",
  rippleColor: "#BBD3F6",

  lightPrimary: "#10B5AF",
  darkPrimary: "#145986",
  primaryColor: "#0087C1",
  darkColor: "#9A9A9A",
  darkLightColor: "#f1f3f5",
  tourchColor: "#e8e8e8",
  placeHolderColor: "#E1E8EC",
  borderColor: "#F4F3F5",
  silverColor: "#F2F0F3",
  greyColor: "#CAC8CC",
  darkGreyColor: "#969696",

  whiteColor: "#FFFFFF",
  yellowColor: "#FFDF00",
  lightYellowColor: "#FBFAD0",
  redColor: "#87031D",
  lightRedColor: "#E23A1A",
  blackColor: "#000000",
  orangeColor: "#F7771D",
  cyanColor: "#1397A8",
  greenColor: "#62D26D",
  lightGreenColor: "#36AF90",
  blueColor: "#002F91",
  lightBlueColor: "#2BA7FC",
  darkBlue: "#646FD4",
  iconColor: "#3949AB",
  skyColor: "#9FC1F3",
  lightBackground: "#EFF3F6",
  darkText: "#333333",
};

const Scale = {
  title: 18,
  icon: 20,
};

const Fonts = {
  kh: "Kantumruy-Regular",
  khBold: "Kantumruy-Bold",
  enBold: "Poppins-Bold",
  enRegular: "Poppins-Regular",
};

export const Theme = {
  Colors,
  Scale,
  Fonts,
};
