export const DEFAULT_STALE_TIME = 3_600_000;

export const QUERY_KEYS = {
  WEB_PUSH_NOTIFICATIONS: "web-push-notifications",
  WIDGET_SETTINGS: "widget-settings",
  WIDGET_INSTALLATION_SCRIPT: "widget-installation-script",
};
