export interface ProfileInterface {
  id: number;
  name: string;
  photo: string;
  role_id: number;
  role_name: string;
  branch_id: number;
}

export interface CountryInterface {
  id: number;
  name: string;
}

export interface UserInterface {
  user: ProfileInterface;
  country: CountryInterface[];
  access_token: string;
}
