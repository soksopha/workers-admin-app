import moment from "moment-timezone";
import "moment/min/locales";
import AsyncStorage from "@react-native-community/async-storage";
import { AccessKey } from "./enums";

let timeZone = "Asia/Bangkok";

const fetchUser = async () => {
  const profile = await AsyncStorage.getItem(AccessKey.PROFILE);
  const response = profile ? JSON.parse(profile) : null;
  return response;
};

const fetchLanguageCode = async () => {
  const language = await AsyncStorage.getItem(AccessKey.LANGUAGE);
  const response = language ? JSON.parse(language) : null;
  return response;
};

const formatDate = (value: string | Date, format = "DD MMM YYYY"): string => {
  return moment(value).tz(timeZone).format(format);
};

const getCurrentDateTime = (format = "YYYY-MM-DD H:mm:ss"): string => {
  return moment().tz(timeZone).format(format);
};

const formatDateForMYSQL = (value: any, format = "YYYY-MM-DD") => {
  format = format === null ? "YYYY-MM-DD" : format;
  value = moment(value).tz(timeZone).format(format);
  return value;
};

const Logout = () => {
  Promise.all([
    AsyncStorage.removeItem(AccessKey.TOKEN),
    AsyncStorage.removeItem(AccessKey.PROFILE),
    AsyncStorage.removeItem(AccessKey.FACTORY)
  ]);
};

const formatCurrency = (
  n: any,
  currency = "$",
  decimal = ".00",
  position = 0,
) => {
  const sign = n < 0 ? "-" : "";
  n = Math.abs(n).toFixed(2);

  if (currency === "៛" || decimal === "") {
    return `${sign}${currency}${n.replace(".00", "")}`;
  }

  const parts = n.split(".");
  const formattedNumber = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  const result =
    position === 0
      ? `${currency}${formattedNumber}.${parts[1]}`
      : `${formattedNumber}.${parts[1]}${currency}`;
  return `${sign}${result}`;
};

export {
  fetchUser,
  fetchLanguageCode,
  formatDate,
  getCurrentDateTime,
  formatDateForMYSQL,
  Logout,
  formatCurrency,
};
