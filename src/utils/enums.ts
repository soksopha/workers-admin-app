export const AccessKey = {
  TOKEN: "ACCESS_TOKEN",
  PROFILE: "PROFILE",
  FACTORY: "FACTORY",
  USER_NAME: "ACCESS_USER",
  ROLE: "ACCESS_ROLE_CODE",
  COUNTRY: "COUNTRY",
  LANGUAGE: "LANGUAGE",
  STORE_REVIEW: 7,
};

export const Gender = {
  MALE: 1,
  FEMALE: 0,
};

export const Language = {
  SCHEMA_KEY: "LANGUAGE",
  LANGUAGE_CODE: {
    EN: 0,
    KM: 1,
  },
};
