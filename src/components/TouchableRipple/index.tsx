import * as React from "react";
import { ViewStyle } from "react-native";
import { TouchableRipple } from "react-native-paper";
import { Theme } from "@src/config/theme";

interface TouchableRippleProps {
  children: React.ReactElement;
  style?: ViewStyle;
  onPress: () => void;
}

const Ripple: React.FC<TouchableRippleProps> = ({
  onPress,
  style,
  children,
}) => (
  <TouchableRipple
    onPress={onPress}
    style={style}
    rippleColor={Theme.Colors.rippleColor}
  >
    {children}
  </TouchableRipple>
);

export default Ripple;
