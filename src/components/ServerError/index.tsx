import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";

interface ServerErrorViewProps {
  isVisible: boolean;
  onClose: () => void;
}

const ServerErrorView: React.FC<ServerErrorViewProps> = ({
  isVisible,
  onClose,
}) => {
  if (!isVisible) {
    return null;
  }

  return (
    <View style={styles.container}>
      <Icon name="exclamation-circle" size={40} color="red" />
      <Text style={styles.message}>
        The server is currently unavailable. Please try again later.
      </Text>
      <TouchableOpacity onPress={onClose} style={styles.closeButton}>
        <Icon name="times" size={20} color="#333" />
        <Text style={styles.closeButtonText}>Close</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
    alignItems: "center",
    justifyContent: "center",
  },
  message: {
    fontSize: 16,
    fontWeight: "bold",
    marginVertical: 10,
  },
  closeButton: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 20,
  },
  closeButtonText: {
    marginLeft: 5,
    fontSize: 16,
    color: "#333",
  },
});

export default ServerErrorView;
