import React, { ReactNode } from "react";
import { useTranslation } from "react-i18next";
import { View } from "react-native";
import { TextInput, Text, HelperText } from "react-native-paper";
import { Controller } from "react-hook-form";
import { StyleSheet, ViewStyle } from "react-native";
import { Theme } from "@src/config/theme";

interface ControlledTextInputProps {
  control: any;
  formState: any;
  name: string;
  rules?: object;
  label: string;
  style?: ViewStyle;
  left?: ReactNode;
  right?: ReactNode;
  secureTextEntry?: boolean;
  placeholderTextColor?: string;
  required?: boolean;
  showUpperCase?: boolean;
  onBlur?: (value: any) => void;
}

const formatDateString = (input: string) => {
  const cleanedInput = input.replace(/[^0-9]/g, "");
  const year = cleanedInput.slice(0, 4);
  const month = cleanedInput.slice(4, 6);
  const day = cleanedInput.slice(6, 8);

  let formattedDate = "";

  if (year) {
    formattedDate += year;
    if (month) {
      formattedDate += `-${month}`;
      if (day) {
        formattedDate += `-${day}`;
      }
    }
  }
  return formattedDate;
};

const DateOfBirthInput: React.FC<ControlledTextInputProps> = ({
  control,
  formState,
  name,
  rules,
  label,
  style,
  left,
  right,
  secureTextEntry,
  placeholderTextColor,
  required,
  onBlur, // Include onBlur in the props
}) => {
  const { t } = useTranslation();
  const { errors } = formState;

  return (
    <View style={style}>
      <Controller
        control={control}
        name={name}
        rules={rules}
        render={({ field: { onBlur: fieldOnBlur, onChange, value } }) => (
          <>
            {label ? (
              <Text style={{ marginBottom: 3 }}>
                {label}{" "}
                <Text style={{ color: Theme.Colors.redColor }}>
                  {required ? "*" : ""}
                </Text>
              </Text>
            ) : null}
            <TextInput
              placeholder={"YYYY-MM-DD"}
              mode="outlined"
              onBlur={(e) => {
                fieldOnBlur();
                if (onBlur) {
                  onBlur(value);
                }
              }}
              onChangeText={(text) => onChange(formatDateString(text))}
              left={left}
              right={right}
              secureTextEntry={secureTextEntry}
              value={value}
              theme={{
                colors: { placeholder: placeholderTextColor || "#999999" },
              }}
              style={styles.customTextInput}
            />
            {errors[name]?.message && (
              <HelperText type="error">
                {errors[name].message + t("required")}
              </HelperText>
            )}
          </>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  customTextInput: {
    height: 50,
    fontSize: 12,
    lineHeight: 20,
  },
});

export default DateOfBirthInput;
