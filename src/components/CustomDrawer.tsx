import React, { useEffect, useState } from "react";
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  StatusBar,
} from "react-native";
import { Text } from "react-native-paper";
import {
  DrawerContentScrollView,
  DrawerItemList,
} from "@react-navigation/drawer";
import { resetNavigation } from "@src/navigation/Navigation";
import Icon from "react-native-vector-icons/Ionicons";
import { Alert } from "@src/components";
import { Theme } from "@src/config/theme";
import { fetchUser, Logout } from "@src/utils/helpers";
import { useTranslation } from "react-i18next";

interface Profile {
  full_name: string;
  organization: string;
}

const CustomDrawer = (props: any) => {
  const { t } = useTranslation();

  const [profile, setProfile] = useState<Profile | null>(null);
  const [isVisible, setIsVisible] = useState(false);

  const handleLogout = () => {
    Logout();
    resetNavigation(props.navigation, "Login");
  };

  useEffect(() => {
    const fetchUserStorage = async () => {
      const response = await fetchUser();
      setProfile(response);
    };

    fetchUserStorage();
  }, []);

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={Theme.Colors.darkPrimary}
        barStyle="light-content"
      />
      <Alert
        visible={isVisible}
        onDismiss={() => setIsVisible(false)}
        onAction={handleLogout}
        title={t("logout_message_title")}
        content={t("logout_message")}
      />
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={styles.drawerContent}
      >
        <View style={styles.header}>
          <Image
            source={require("../assets/images/app_light_logo.png")}
            style={styles.profileImage}
          />
          <Text style={styles.profileName}>{profile?.full_name}</Text>
          <View style={styles.profileSubtitleContainer}>
            <Text style={styles.profileSubtitle}>{profile?.organization}</Text>
          </View>
        </View>
        <View style={styles.drawerItemsContainer}>
          <DrawerItemList {...props} />
        </View>
      </DrawerContentScrollView>
      <View style={styles.logoutContainer}>
        <TouchableOpacity onPress={() => setIsVisible(true)}>
          <View style={styles.logoutButton}>
            <Icon name="exit-outline" size={22} />
            <Text style={styles.logoutText}>{t("logout_title")}</Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  drawerContent: {
    backgroundColor: Theme.Colors.darkPrimary,
  },
  header: {
    padding: 20,
  },
  profileImage: {
    height: 80,
    width: 80,
    borderRadius: 40,
    marginBottom: 10,
  },
  profileName: {
    color: "#fff",
    fontSize: 18,
    marginBottom: 5,
  },
  profileSubtitleContainer: {
    flexDirection: "row",
  },
  profileSubtitle: {
    color: "#fff",
    marginRight: 5,
  },
  drawerItemsContainer: {
    flex: 1,
    backgroundColor: "#fff",
    paddingTop: 10,
  },
  logoutContainer: {
    padding: 20,
    borderTopWidth: 1,
    borderTopColor: "#ccc",
  },
  logoutButton: {
    flexDirection: "row",
    alignItems: "center",
  },
  logoutText: {
    fontSize: 15,
    marginLeft: 5,
  },
});

export default CustomDrawer;
