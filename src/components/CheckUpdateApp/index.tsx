import React, { useEffect, useState } from "react";
import VersionCheck from "react-native-version-check";
import { Button } from "react-native-paper";
import {
  View,
  StatusBar,
  Linking,
  Modal,
  Image,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Platform,
} from "react-native";
import { useTranslation } from "react-i18next";
import { Text } from "react-native-paper";
import { Theme } from "@src/config/theme";

const iosImage = require("@src/assets/images/app-store.png");
const androidImage = require("@src/assets/images/google-play.png");
const updateAppImage = Platform.OS === "android" ? androidImage : iosImage;

interface CheckUpdateAppProps {
  handleRequestClose: () => void;
}

const CheckUpdateApp: React.FC<CheckUpdateAppProps> = ({
  handleRequestClose,
}) => {
  const { t } = useTranslation();
  const [visible, setVisible] = useState(true);
  const [storeUrl, setStoreUrl] = useState<string | undefined>();

  useEffect(() => {
    checkUpdateNeeded();
  }, []);

  const checkUpdateNeeded = async () => {
    try {
      const updateNeed = await VersionCheck.needUpdate();
      if (updateNeed && updateNeed.isNeeded) {
        setVisible(true);
        setStoreUrl(updateNeed.storeUrl);
      }
    } catch (error) {
      console.error(error);
    }
  };

  const onRequestClose = () => {
    setVisible(false);
    handleRequestClose();
  };

  const handleUpdateApp = () => {
    if (storeUrl) {
      Linking.openURL(storeUrl);
      setVisible(false);
    }
  };

  return (
    <Modal animationType="fade" visible={visible} statusBarTranslucent={true}>
      <StatusBar animated={true} showHideTransition={"fade"} />
      <View style={styles.modalContainer}>
        <View style={styles.container}>
          <View style={styles.contentContainer}>
            <Image source={updateAppImage} style={styles.logoImage} />
            <Text style={styles.title}>{t("timeToUpdate")}</Text>
            <Text style={styles.subTitle}>{t("timeToUpdateDesc")}</Text>
            <Button
              style={styles.updateButton}
              labelStyle={styles.buttonLabel}
              mode="contained"
              onPress={handleUpdateApp}
            >
              {t("updateNow")}
            </Button>
            <TouchableOpacity onPress={onRequestClose}>
              <Text style={styles.notNowText}>{t("notNow")}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const { width } = Dimensions.get("window");
const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    backgroundColor: Theme.Colors.primaryColor,
  },
  container: {
    flex: 1,
    width: width,
    justifyContent: "center",
    alignItems: "center",
  },
  contentContainer: {
    alignItems: "center",
    padding: 20,
    backgroundColor: Theme.Colors.whiteColor,
    borderRadius: 10,
    elevation: 5,
  },
  logoImage: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
    color: Theme.Colors.primary,
  },
  subTitle: {
    textAlign: "center",
    lineHeight: 20,
    fontSize: 14,
    color: Theme.Colors.blackColor,
    marginBottom: 20,
  },
  updateButton: {
    borderRadius: 10,
    height: 45,
    width: 200,
    backgroundColor: Theme.Colors.primary,
    marginTop: 10,
  },
  buttonLabel: {
    color: Theme.Colors.whiteColor,
  },
  notNowText: {
    color: Theme.Colors.primary,
    marginTop: 20,
    fontSize: 14,
  },
});

export default CheckUpdateApp;
