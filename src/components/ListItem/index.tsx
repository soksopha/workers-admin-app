import React from "react";
import { scale } from "react-native-size-matters";
import { useTranslation } from "react-i18next";
import { View, StyleSheet } from "react-native";
import { Card, Avatar, Text } from "react-native-paper";
import { Theme } from "@src/config/theme";
import { navigateTo } from "@src/navigation/Navigation";

import { IMAGE_PATH } from "@env";

interface ItemProps {
  item: {
    id: string;
    name_kh?: string;
    is_black_list?: string;
    no_doc?: string;
    color?: string;
    name_en?: string;
    phone_number?: string;
    photo?: string;
  };
  navigation: any;
}

const ListItems: React.FC<ItemProps> = ({ item, navigation }) => {
  const { t } = useTranslation();
  return (
    <Card
      onPress={() =>
        navigateTo(navigation, "HistoryWorker", {
          id: item?.id,
        })
      }
      style={{ marginBottom: 10 }}
    >
      <Card.Content>
        <View style={{ flexDirection: "row" }}>
          <Avatar.Image
            size={60}
            source={
              item?.photo
                ? { uri: `${IMAGE_PATH}/${item?.photo}` }
                : require("@src/assets/images/avatar.png")
            }
          />
          <View
            style={{ flexDirection: "row", justifyContent: "space-between" }}
          >
            <View style={{ marginLeft: 20 }}>
              <View
                style={{
                  flexDirection: "row",
                  width: scale(160),
                  justifyContent: "space-between",
                  flex: 1,
                }}
              >
                <Text style={styles.titleText}>{item?.name_kh || "N/A"}</Text>
              </View>
              <Text style={styles.titleText}>{item?.name_en || "N/A"}</Text>
              <Text style={styles.descText}>{item?.phone_number || "N/A"}</Text>
            </View>
            <View>
              {item?.is_black_list == "1" ? (
                <View
                  style={[
                    styles.showBlackList,
                    { borderColor: item?.color, marginBottom: 10 },
                  ]}
                >
                  <Text style={[styles.textColor, { color: item?.color }]}>
                    {t("blackListText")}
                  </Text>
                </View>
              ) : (
                ""
              )}

              {item?.no_doc == "1" ? (
                <View
                  style={[
                    styles.showBlackList,
                    { borderColor: Theme.Colors.redColor },
                  ]}
                >
                  <Text
                    style={[styles.textColor, { color: Theme.Colors.redColor }]}
                  >
                    {t("removeText")}
                  </Text>
                </View>
              ) : (
                ""
              )}
            </View>
          </View>
        </View>
      </Card.Content>
    </Card>
  );
};

const styles = StyleSheet.create({
  titleText: {
    color: Theme.Colors.blackColor,
    lineHeight: 25,
  },
  descText: {
    color: Theme.Colors.darkColor,
    lineHeight: 25,
  },
  showBlackList: {
    // position: "absolute",
    // right: 10,
    borderRadius: 5,
    // borderColor: item?.color,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    height: 25,
    width: 70,
  },
  textColor: {
    fontSize: 10,
    textAlign: "center",
    // color: item?.color,
  },
});

export default ListItems;
