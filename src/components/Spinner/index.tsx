import * as React from "react";
import { ActivityIndicator } from "react-native-paper";
import { Theme } from "@src/config/theme";

const Spinner: React.FC = () => (
  <ActivityIndicator animating={true} color={Theme.Colors.primary} />
);

export default Spinner;
