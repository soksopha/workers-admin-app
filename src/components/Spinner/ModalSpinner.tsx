import { Theme } from "@src/config/theme";
import React from "react";
import { View, Modal, ActivityIndicator, StyleSheet } from "react-native";

interface ModalSpinnerProps {
  visible: boolean;
}

const ModalSpinner: React.FC<ModalSpinnerProps> = ({ visible }) => {
  return (
    <Modal transparent={true} animationType="none" visible={visible}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator
            animating={visible}
            size="large"
            color={Theme.Colors.primary}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    backgroundColor: "rgba(0, 0, 0, 0.5)",
    justifyContent: "center",
    alignItems: "center",
  },
  activityIndicatorWrapper: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
});

export default ModalSpinner;
