import React, { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import {
  View,
  Text,
  StyleSheet,
  Modal,
  TouchableOpacity,
  BackHandler,
} from "react-native";
import NetInfo, { NetInfoState } from "@react-native-community/netinfo";
import Icon from "react-native-vector-icons/FontAwesome";
import { Theme } from "@src/config/theme";

const NoInternetMessage = () => {
  const { t } = useTranslation();
  const [isConnected, setIsConnected] = useState<boolean | null>(true);

  useEffect(() => {
    const unsubscribe = NetInfo.addEventListener((state: NetInfoState) => {
      setIsConnected(state.isConnected);
    });

    return () => {
      unsubscribe();
    };
  }, []);

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={!isConnected}
      onRequestClose={() => {
        BackHandler.exitApp();
      }}
    >
      <View style={styles.centeredView}>
        <View style={styles.modalView}>
          <Icon name="wifi" size={60} color={Theme.Colors.whiteColor} />
          <Text style={styles.modalText}>{t("connectionMessage")}</Text>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => {
              BackHandler.exitApp();
            }}
          >
            <Text style={styles.closeText}>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)",
  },
  modalView: {
    backgroundColor: Theme.Colors.danger,
    borderRadius: 20,
    padding: 40,
    alignItems: "center",
    elevation: 5,
  },
  modalText: {
    color: Theme.Colors.whiteColor,
    textAlign: "center",
    marginTop: 20,
    fontSize: 18,
  },
  closeButton: {
    marginTop: 30,
  },
  closeText: {
    color: Theme.Colors.whiteColor,
    fontWeight: "bold",
  },
});

export default NoInternetMessage;
