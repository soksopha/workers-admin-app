import React from "react";
import { Snackbar, Text } from "react-native-paper";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Theme } from "@src/config/theme";

interface SnackbarProps {
  visible: boolean;
  onDismiss: () => void;
  message?: string;
  type?: any;
}

const CustomSnackbar: React.FC<SnackbarProps> = ({
  visible,
  onDismiss,
  type = "error",
  message,
}) => {
  const typeMap: Record<string, { backgroundColor: string; iconName: string }> =
    {
      success: {
        backgroundColor: Theme.Colors.greenColor,
        iconName: "check-circle",
      },
      error: {
        backgroundColor: Theme.Colors.lightRedColor,
        iconName: "exclamation-circle",
      },
      warning: {
        backgroundColor: Theme.Colors.active,
        iconName: "exclamation-triangle",
      },
    };
  const { backgroundColor, iconName } = typeMap[type] || typeMap.warning;

  return (
    <Snackbar
      visible={visible}
      onDismiss={onDismiss}
      style={{ backgroundColor: backgroundColor, zIndex: 9999 }}
      action={{
        label: "Close",
        onPress: onDismiss,
      }}
    >
      <Text style={{ color: Theme.Colors.whiteColor }}>
        <Icon name={iconName} color={Theme.Colors.whiteColor} size={14} />
        &nbsp;&nbsp;{message}
      </Text>
    </Snackbar>
  );
};

export default CustomSnackbar;
