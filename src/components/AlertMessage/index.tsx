import { useEffect } from "react";
import { t } from "i18next";
import { Theme } from "@src/config/theme";
import React from "react";
import { useNavigation } from "@react-navigation/native";
import {
  ALERT_TYPE,
  AlertNotificationRoot,
  Dialog,
  Toast,
} from "react-native-alert-notification";
export { ALERT_TYPE, Dialog, Toast };

interface DialogMessageProps {
  mutation: {
    isSuccess: boolean;
    isError: boolean;
  };
  onHideSuccess?: void;
  onHideError?: void;
}

export function DialogMessage({
  mutation,
  onHideSuccess,
  onHideError,
}: DialogMessageProps) {
  const navigation = useNavigation();
  useEffect(() => {
    if (mutation.isSuccess) {
      Dialog.show({
        type: ALERT_TYPE.SUCCESS,
        title: t("success"),
        textBody: t("successSaveRecord"),
        button: t("ok"),
        autoClose: 1000,
      });
    } else if (mutation.isError) {
      Dialog.show({
        type: ALERT_TYPE.DANGER,
        title: t("error"),
        textBody: t("someThingWhenWrong"),
        button: t("ok"),
        autoClose: 1000,
      });
    }
  }, [mutation.isSuccess, mutation.isError]);

  return null;
}

export default function AlertMessage({ children }: { children: JSX.Element }) {
  return (
    <AlertNotificationRoot
      toastConfig={{
        titleStyle: {
          fontFamily: Theme.Fonts.khBold,
        },
        textBodyStyle: {
          fontFamily: Theme.Fonts.kh,
        },
      }}
    >
      {children}
    </AlertNotificationRoot>
  );
}
