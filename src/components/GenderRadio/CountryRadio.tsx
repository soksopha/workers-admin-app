import * as React from "react";
import { useTranslation } from "react-i18next";
import { View, StyleSheet } from "react-native";
import { RadioButton, Text, HelperText } from "react-native-paper";
import { Controller } from "react-hook-form";
import { Theme } from "@src/config/theme";

type RadioProps = {
  formState: any;
  control: any;
  name: string;
  rules?: object;
  required?: boolean;
};

const CountryRadio: React.FC<RadioProps> = ({
  formState,
  control,
  name,
  rules,
  required,
}) => {
  const { t } = useTranslation();
  const { errors } = formState;
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field: { onBlur, onChange, value } }) => (
        <RadioButton.Group
          onValueChange={(newValue) => onChange(newValue)}
          value={value}
        >
          <View
            style={{
              flexDirection: "row",
              marginLeft: -10,
              height: 50,
              alignItems: "center",
            }}
          >
            <View style={[styles.radioContainer, { marginRight: 20 }]}>
              <RadioButton value="1" color={Theme.Colors.primary} />
              <Text>{t("blackListByCountry")}</Text>
            </View>
            <View style={styles.radioContainer}>
              <RadioButton value="2" color={Theme.Colors.primary} />
              <Text>{t("blackListByCompany")}</Text>
            </View>
          </View>
          {errors[name]?.message && (
            <HelperText type="error">{errors[name].message}</HelperText>
          )}
        </RadioButton.Group>
      )}
    />
  );
};

const styles = StyleSheet.create({
  radioContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default CountryRadio;
