import * as React from "react";
import { useTranslation } from "react-i18next";
import { View, StyleSheet } from "react-native";
import { RadioButton, Text, HelperText } from "react-native-paper";
import { Controller } from "react-hook-form";
import { Theme } from "@src/config/theme";

type RadioProps = {
  formState: any;
  control: any;
  name: string;
  rules?: object;
  label: string;
  required?: boolean;
};

const GenderRadio: React.FC<RadioProps> = ({
  formState,
  control,
  name,
  rules,
  required,
  label,
}) => {
  const { t } = useTranslation();
  const { errors } = formState;
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field: { onBlur, onChange, value } }) => (
        <RadioButton.Group
          onValueChange={(newValue) => onChange(newValue)}
          value={value}
        >
          {label ? (
            <Text style={{ marginBottom: 0 }}>
              {label}{" "}
              <Text style={{ color: Theme.Colors.redColor }}>
                {required ? "*" : ""}
              </Text>
            </Text>
          ) : null}
          <View
            style={{ flexDirection: "row", height: 50, alignItems: "center" }}
          >
            <View style={styles.radioContainer}>
              <Text>{t("male")}</Text>
              <RadioButton value="1" color={Theme.Colors.primary} />
            </View>
            <View style={styles.radioContainer}>
              <Text>{t("female")}</Text>
              <RadioButton value="2" color={Theme.Colors.primary} />
            </View>
          </View>
          {errors[name]?.message && (
            <HelperText type="error">{errors[name].message + label}</HelperText>
          )}
        </RadioButton.Group>
      )}
    />
  );
};

const styles = StyleSheet.create({
  radioContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default GenderRadio;
