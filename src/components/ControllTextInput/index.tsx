import React, { ReactNode, FocusEvent } from "react";
import { View } from "react-native";
import { TextInput, Text, DefaultTheme, HelperText } from "react-native-paper";
import { Controller, FieldValues } from "react-hook-form";
import { StyleSheet, ViewStyle } from "react-native";
import { Theme } from "@src/config/theme";

interface ControlledTextInputProps {
  control: any;
  formState: any;
  name: string;
  rules?: object;
  label: string;
  style?: ViewStyle;
  left?: ReactNode;
  secureTextEntry?: boolean;
  required?: boolean;
  disabled?: boolean;
  onBlur?: (e: FocusEvent<any> | React.FocusEvent<HTMLInputElement>) => void;
}

const ControlledTextInput: React.FC<ControlledTextInputProps> = ({
  control,
  formState,
  name,
  rules,
  label,
  style,
  left,
  secureTextEntry,
  required,
  disabled = false,
  onBlur, // Use the onBlur prop
}) => {
  const { errors } = formState;
  const customTheme: any = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      placeholder: Theme.Colors.borderColor,
    },
    fonts: {
      regular: {
        fontFamily: Theme.Fonts.kh,
      },
    },
  };

  return (
    <View style={style}>
      <Controller
        control={control}
        name={name}
        disabled
        rules={rules}
        render={({ field: { onBlur: fieldOnBlur, onChange, value } }) => (
          <>
            {label ? (
              <Text style={{ marginBottom: 3 }}>
                {label}{" "}
                <Text style={{ color: Theme.Colors.redColor }}>
                  {required ? "*" : ""}
                </Text>
              </Text>
            ) : null}
            <TextInput
              placeholder={label}
              mode="outlined"
              onBlur={(e) => {
                fieldOnBlur();
                if (onBlur) {
                  onBlur(e);
                }
              }}
              onChangeText={onChange}
              left={left}
              secureTextEntry={secureTextEntry}
              value={value}
              disabled={disabled}
              theme={{
                colors: { placeholder: "#999999" },
              }}
              style={styles.customTextInput}
            />
            {errors[name]?.message && (
              <HelperText type="error">{errors[name].message}</HelperText>
            )}
          </>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  customTextInput: {
    height: 50,
    fontSize: 12,
    lineHeight: 20,
    backgroundColor: Theme.Colors.whiteColor,
  },
});

export default ControlledTextInput;
