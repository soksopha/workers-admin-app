import React from "react";
import { useTranslation } from "react-i18next";
import { StyleSheet } from "react-native";
import { Text, Button, Dialog, Portal } from "react-native-paper";
import { Theme } from "@src/config/theme";

interface AlertComponentProps {
  visible: boolean;
  onDismiss: () => void;
  onAction: () => void;
  title: string;
  content: string;
  contentStyle?: any;
  dismissText?: string;
  actionText?: string;
}

const Alert: React.FC<AlertComponentProps> = ({
  visible,
  onDismiss,
  onAction,
  title,
  content,
  contentStyle,
  dismissText,
  actionText,
}) => {
  const { t } = useTranslation();
  return (
    <Portal>
      <Dialog visible={visible} onDismiss={onDismiss}>
        <Dialog.Title style={{ fontSize: 17 }}>{title}</Dialog.Title>
        <Dialog.Content>
          <Text style={contentStyle}>{content}</Text>
        </Dialog.Content>
        <Dialog.Actions>
          <Button textColor={Theme.Colors.darkPrimary} onPress={onDismiss}>
            {dismissText || t("cancel")}
          </Button>
          <Button textColor={Theme.Colors.darkPrimary} onPress={onAction}>
            {actionText || t("ok")}
          </Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
};

const styles = StyleSheet.create({
  buttonStyle: {
    color: Theme.Colors.primaryColor,
  },
});

export default Alert;
