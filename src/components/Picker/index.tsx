import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { Controller } from "react-hook-form";
import {
  StyleSheet,
  View,
  ScrollView,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import { Text, HelperText, Title } from "react-native-paper";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Theme } from "@src/config/theme";

const { height } = Dimensions.get("window");
interface PickerSelectProps {
  control: any;
  formState: any;
  name: string;
  label: string;
  placeholder: string;
  handleChange: (id: any) => void;
  datas: { id: any; name_kh: string; name_en: string }[];
  selectedId?: any;
  rules?: any;
  disabled?: boolean
}

const PickerSelect: React.FC<PickerSelectProps> = ({
  control,
  formState,
  name,
  label,
  handleChange,
  datas,
  selectedId,
  rules,
  disabled = false
}) => {
  const { i18n } = useTranslation();
  const currentLanguage = i18n.language === "en";
  const { errors } = formState;
  const [stateSelectedId, setStateSelectedId] = useState(selectedId);
  const [isModalVisible, setIsModalVisible] = useState(false);

  const toggleModal = () => {
    setIsModalVisible(!isModalVisible);
  };

  const handleSelection = (item: { id: any }) => {
    handleChange(item.id);
    toggleModal();
    setStateSelectedId(item.id);
  };

  const active = selectedId || stateSelectedId;
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({ field: { onBlur, onChange, value } }) => (
        <View style={styles.container}>
          <Text style={{ marginBottom: 4 }}>{label}</Text>
          <TouchableOpacity disabled={disabled ? true : false} style={styles.button} onPress={toggleModal}>
            <View
              style={{ width: 220, flexDirection: "row", alignItems: "center" }}
            >
              <Text style={styles.buttonText}>
                {active
                  ? currentLanguage
                    ? datas.find((item) => item.id === active)?.name_en
                    : datas.find((item) => item.id === active)?.name_kh
                  : label}
              </Text>
            </View>
            {disabled ? null : <Icon name="caret-down" size={22} />}
          </TouchableOpacity>

          {errors[name]?.message && (
            <HelperText type="error">{errors[name].message}</HelperText>
          )}

          <Modal
            backdropOpacity={0.5}
            isVisible={isModalVisible}
            onBackdropPress={toggleModal}
            onBackButtonPress={toggleModal}
            animationIn="slideInUp"
          >
            <View style={styles.modal}>
              <Title
                style={{ lineHeight: 40, fontSize: 16, textAlign: "center" }}
              >
                {label}
              </Title>
              <ScrollView>
                {datas && datas.map((item) => (
                  <TouchableOpacity
                    style={[
                      styles.modalItem,
                      item.id == active ? styles.selectedItem : null,
                    ]}
                    key={item.id}
                    onPress={() => {
                      handleSelection(item);
                      onChange(item.id);
                      onBlur();
                    }}
                  >
                    <Text
                      style={[
                        styles.modalItemText,
                        item.id == active
                          ? styles.selectedText
                          : styles.unSelectedText,
                      ]}
                    >
                      {currentLanguage ? item.name_en : item.name_kh}
                    </Text>
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
          </Modal>
        </View>
      )}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
  },
  button: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: Theme.Colors.whiteColor,
    borderColor: Theme.Colors.darkGreyColor,
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
    borderWidth: 1,
  },
  buttonText: {
    fontSize: 14,
    color: Theme.Colors.blackColor,
    fontFamily: Theme.Fonts.kh,
  },
  modal: {
    backgroundColor: "white",
    borderRadius: 5,
    padding: 10,
    height: height - 200,
  },
  modalItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: Theme.Colors.border,
  },
  modalItemText: {
    fontSize: 14,
    fontFamily: Theme.Fonts.kh,
  },
  selectedItem: {
    backgroundColor: Theme.Colors.primary,
    color: Theme.Colors.whiteColor,
    fontWeight: "bold",
  },
  selectedText: {
    color: Theme.Colors.whiteColor,
  },
  unSelectedText: {
    color: Theme.Colors.blackColor,
  },
});

export default PickerSelect;
