import React, { useEffect, useState } from "react";
import SplashScreen from "react-native-splash-screen";
import { View, StyleSheet } from "react-native";
import VersionCheck from "react-native-version-check";
import { useTranslation } from "react-i18next";
import { CheckUpdateApp } from "@src/components";
import { resetNavigation, navigateTo } from "@src/navigation/Navigation";
import { fetchUser, fetchLanguageCode } from "@src/utils/helpers";

interface AuthLoadingProps {
  navigation: any;
}

const AuthLoadingScreen: React.FC<AuthLoadingProps> = ({ navigation }) => {
  const { i18n } = useTranslation();
  const [updateNeeded, setUpdateNeeded] = useState<boolean>(false);

  const fetchUserStorage = async () => {
    const response = await fetchUser();
    const language = await fetchLanguageCode();

    if (!language) {
      navigateTo(navigation, "SelectLanguage");
      return;
    }

    i18n.changeLanguage(language);

    if (response) {
      resetNavigation(navigation, "App");
    } else {
      resetNavigation(navigation, "Login");
    }
  };

  const handleNotUpdate = async () => {
    resetNavigation(navigation, "App");
  };

  const checkUpdateAndNavigate = async () => {
    try {
      const updateNeed = await VersionCheck.needUpdate();
      setUpdateNeeded(updateNeed?.isNeeded);
      if (!updateNeed?.isNeeded) {
        fetchUserStorage();
      }
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchUserStorage();
    setTimeout(() => {
      SplashScreen.hide();
    }, 500);
  }, []);

  return <></>;
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default AuthLoadingScreen;
