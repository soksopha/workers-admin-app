import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import AsyncStorage from "@react-native-community/async-storage";
import Icon from "react-native-vector-icons/FontAwesome5";
import { useMutation } from "@tanstack/react-query";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { View, Image, StyleSheet } from "react-native";
import { Title, TextInput, Button } from "react-native-paper";
import { Snackbar, ControlledTextInput } from "@src/components";
import { resetNavigation } from "@src/navigation/Navigation";
import { Theme } from "@src/config/theme";
import { login } from "@src/services/authApi";
import { AccessKey } from "@src/utils/enums";

interface LoginScreenProps {
  navigation: any;
}

const LoginScreen: React.FC<LoginScreenProps> = ({ navigation }) => {
  const { t } = useTranslation();
  const [message, setMessage] = useState<string>("");
  const [visible, setVisible] = useState(false);
  const { handleSubmit, control, formState } = useForm<FormData>();
  const mutation = useMutation({
    mutationFn: login,
    onError: (error, variables, context) => {
      setMessage(t("someThingWhenWrong"));
      setVisible(true);
    },
    onSuccess: (data: any) => {
      if (data) {
        Promise.all([
          AsyncStorage.setItem(AccessKey.TOKEN, data.token),
          AsyncStorage.setItem(AccessKey.PROFILE, JSON.stringify(data.user)),
          AsyncStorage.setItem(AccessKey.FACTORY, JSON.stringify(data.factory))
        ]).then(() => {
          resetNavigation(navigation, "App");
        });
      } else {
        setVisible(true);
        setMessage(t("invalidUserNamePassword"));
      }
    },
  });

  const onSubmit = (data: any) => {
    const { username, password } = data;
    mutation.mutate({ username, password });
  };

  return (
    <KeyboardAwareScrollView
      keyboardShouldPersistTaps="handled"
      contentContainerStyle={styles.container}
    >
      <Snackbar
        onDismiss={() => setVisible(false)}
        visible={visible}
        message={message}
      />

      <Image
        source={require("@src/assets/images/app_logo.png")}
        style={styles.image}
        resizeMode="contain"
      />
      <Title style={styles.title}>{t("title")}</Title>
      <View style={styles.form}>
        <ControlledTextInput
          control={control}
          formState={formState}
          name="username"
          label={t("userName")}
          left={
            <TextInput.Icon icon={() => <Icon name="user-alt" size={20} />} />
          }
          rules={{ required: t("userRequired") }}
          style={styles.input}
        />

        <ControlledTextInput
          control={control}
          formState={formState}
          name="password"
          label={t("password")}
          left={<TextInput.Icon icon={() => <Icon name="lock" size={20} />} />}
          secureTextEntry
          rules={{ required: t("passwordIsRequired") }}
          style={styles.input}
        />

        <Button
          mode="contained"
          loading={mutation.isPending}
          disabled={mutation.isPending}
          theme={{ colors: { primary: Theme.Colors.primary } }}
          onPress={handleSubmit(onSubmit)}
          contentStyle={{ height: 50 }}
          style={styles.button}
        >
          {t("login")}
        </Button>
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    padding: 20,
  },
  image: {
    width: 120,
    height: 120,
    marginBottom: 24,
  },
  title: {
    fontSize: 18,
    marginBottom: 20,
  },
  subTitle: {
    fontSize: 16,
    marginBottom: 20,
  },
  form: {
    width: "100%",
  },
  input: {
    marginBottom: 16,
  },
  button: {
    width: "100%",
  },
});

export default LoginScreen;
