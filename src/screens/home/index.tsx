import React, { useState } from "react";
import Modal from "react-native-modal";
import { useForm } from "react-hook-form";
import {
    ScrollView,
    TouchableOpacity,
    Dimensions,
    RefreshControl,
    View,
    StyleSheet,
} from "react-native";
import Icon from "react-native-vector-icons/Feather";
import { useFetchDashboard } from "@src/hooks/useDashboard";
import {
    useFilterList,
    useFactorList,
    useBranchList,
} from "@src/hooks/useFilters";
import { useStorage } from "@src/hooks/useLocalStorage";
import { ModalSpinner, PickerSelect } from "@src/components";
import { useTranslation } from "react-i18next";
import { Text, Title, Button, Avatar } from "react-native-paper";
import { Theme } from "@src/config/theme";
import { navigateTo } from "@src/navigation/Navigation";
import { AccessKey } from "@src/utils/enums";
import { IMAGE_PATH } from "@env";

const { height, width } = Dimensions.get("window");
const Home: React.FC = ({ navigation }: any) => {
    const { t } = useTranslation();
    const { data: factory } = useStorage(AccessKey.FACTORY);
    const [visible, setVisible] = useState<boolean>(false);
    const { reset, control, formState } = useForm({
        defaultValues: {
            company: factory?.country_id,
            branch: factory?.factory_id,
        },
    });
    const { data: user } = useStorage(AccessKey.PROFILE);
    const [selectCountry, setSelectCountry] = useState<number>(0);
    const [selectCompany, setSelectCompany] = useState<number>(0);
    const [selectBranch, setSelectBranch] = useState<number>(0);
    const [params, setParams] = useState<any>({
        selectCountry: "",
        selectCompany: "",
        selectBranch: "",
    });
    const { data: filterLists } = useFilterList();
    const { data: filterFactor } = useFactorList(
        Number(factory?.country_id || selectCountry)
    );
    const { data: filterBranch } = useBranchList(
        Number(factory?.factory_id || selectCompany)
    );
    const { data, isLoading, refetch, isFetching } = useFetchDashboard({
        country: params?.selectCountry,
        factory: params?.selectCompany,
        branch: params?.selectBranch,
    });

    const { countries } = filterLists || {};
    const RenderItem = ({
        backgroundColor,
        summaryText,
        totalSummary,
        iconName,
        processStatus,
    }: any) => (
        <TouchableOpacity
            onPress={() =>
                navigateTo(navigation, "FilterWorker", {
                    title: summaryText,
                    summaryText,
                    totalSummary,
                    processStatus,
                })
            }
        >
            <View style={[styles.cardContainer, { backgroundColor }]}>
                <View style={styles.workerContainer}>
                    <View style={{ flexDirection: "row" }}>
                        <Icon
                            size={30}
                            color={Theme.Colors.whiteColor}
                            name={iconName}
                            style={{ marginRight: 10 }}
                        />
                        <View>
                            <Title style={{ color: Theme.Colors.whiteColor }}>
                                {totalSummary}
                            </Title>
                            <View style={{ width: width / 3 - 10 }}>
                                <Text
                                    style={{
                                        color: Theme.Colors.whiteColor,
                                        fontSize: 14,
                                    }}
                                >
                                    {summaryText}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );

    const RenderFullItem = ({
        backgroundColor,
        summaryText,
        totalSummary,
        iconName,
        processStatus,
    }: any) => (
        <TouchableOpacity
            onPress={() =>
                navigateTo(navigation, "FilterWorker", {
                    title: summaryText,
                    summaryText,
                    totalSummary,
                    processStatus,
                })
            }
        >
            <View style={[styles.cardFullContainer, { backgroundColor }]}>
                <View style={styles.workerContainer}>
                    <View style={{ flexDirection: "row" }}>
                        <Icon
                            size={30}
                            color={Theme.Colors.whiteColor}
                            name={iconName}
                            style={{ marginRight: 10 }}
                        />
                        <View>
                            <Title style={{ color: Theme.Colors.whiteColor }}>
                                {totalSummary}
                            </Title>
                            <View style={{ width: width / 3 - 10 }}>
                                <Text
                                    style={{
                                        color: Theme.Colors.whiteColor,
                                        fontSize: 14,
                                    }}
                                >
                                    {summaryText}
                                </Text>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );

    const handleRefetch = () => {
        setParams({
            selectCountry: "",
            selectCompany: "",
            selectBranch: "",
        });
        reset();
        setSelectCountry(0);
        setSelectCompany(0);
        refetch();
    };

    if (isLoading) {
        return <ModalSpinner visible />;
    }

    return (
        <ScrollView
            contentContainerStyle={styles.scrollContainer}
            refreshControl={
                <RefreshControl refreshing={isFetching} onRefresh={refetch} />
            }
        >
            <View
                style={[
                    styles.container,
                    { marginVertical: 10, paddingHorizontal: 4 },
                ]}
            >
                <View>
                    <Text>{t("hello")}</Text>
                    <Title>{user?.name}</Title>
                </View>
                <View style={{ flexDirection: "row", display: "flex" }}>
                    {user?.role_id != 7 ? (
                        <TouchableOpacity onPress={() => setVisible(true)}>
                            <Avatar.Icon
                                size={46}
                                style={{ marginRight: 10 }}
                                color={Theme.Colors.borderColor}
                                icon="filter"
                            />
                        </TouchableOpacity>
                    ) : null}
                    <TouchableOpacity
                        onPress={() => navigateTo(navigation, "setting")}
                    >
                        <Avatar.Image
                            size={46}
                            style={{
                                marginBottom: 20,
                                backgroundColor: Theme.Colors.borderColor,
                            }}
                            source={
                                user?.photo
                                    ? { uri: `${IMAGE_PATH}/${user.photo}` }
                                    : require("@src/assets/images/avatar.png")
                            }
                        />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.container}>
                <RenderFullItem
                    backgroundColor="#D37393"
                    iconName="users"
                    summaryText={t("totalWorker")}
                    totalSummary={data?.summary?.total?.value || 0}
                    processStatus={data?.summary?.total?.process_status}
                />
            </View>
            <View style={styles.container}>
                <RenderItem
                    backgroundColor="#AE3816"
                    iconName="x-circle"
                    summaryText={t("cancelWorker")}
                    totalSummary={data?.summary?.canceled?.value || 0}
                    processStatus={data?.summary?.canceled?.process_status}
                />
                <RenderItem
                    backgroundColor="#9C6498"
                    iconName="user-plus"
                    summaryText={t("totalRemainWorker")}
                    totalSummary={data?.summary?.remain?.value || 0}
                    processStatus={data?.summary?.remain?.process_status}
                />
            </View>
            <View style={styles.container}>
                <RenderItem
                    backgroundColor="#70919C"
                    iconName="inbox"
                    summaryText={t("totalPreparedDocument")}
                    totalSummary={data?.summary?.prepare_doc?.value || 0}
                    processStatus={data?.summary?.prepare_doc?.process_status}
                />
                <RenderItem
                    backgroundColor="#8689AD"
                    iconName="truck"
                    summaryText={t("totalNumberWorkerReady")}
                    totalSummary={data?.summary?.prepare_dep?.value || 0}
                    processStatus={data?.summary?.prepare_dep?.process_status}
                />
            </View>
            <View style={styles.container}>
                <RenderItem
                    backgroundColor="#C1A354"
                    iconName="alert-octagon"
                    summaryText={t("totalUnqualified")}
                    totalSummary={data?.summary?.not_leave?.value || 0}
                    processStatus={data?.summary?.not_leave?.process_status}
                />
                <RenderItem
                    backgroundColor="#6E9C59"
                    iconName="map-pin"
                    summaryText={t("totalReachTarget")}
                    totalSummary={data?.summary?.arrival?.value || 0}
                    processStatus={data?.summary?.arrival?.process_status}
                />
            </View>
            <View style={styles.listCardcontainer}>
                <View style={styles.childCardContainer}>
                    <View style={{ flexDirection: "row" }}>
                        <Icon size={20} name="users" />
                        <Text style={{ marginLeft: 20 }}>
                            {t("numberOfNewWorkerThisMonth")}
                        </Text>
                    </View>
                    <View>
                        <Title>+{data?.by_month?.new || 0}</Title>
                    </View>
                </View>
                <View style={styles.childCardContainer}>
                    <View style={{ flexDirection: "row" }}>
                        <Icon size={20} name="user-plus" />
                        <Text style={{ marginLeft: 20 }}>
                            {t("numberRemainWorkerThisMonth")}
                        </Text>
                    </View>
                    <View>
                        <Title>+{data?.by_month?.remain || 0}</Title>
                    </View>
                </View>
                <View
                    style={[
                        styles.childCardContainer,
                        { borderBottomWidth: 0 },
                    ]}
                >
                    <View style={{ flexDirection: "row" }}>
                        <Icon size={20} name="alert-octagon" />
                        <Text style={{ marginLeft: 20 }}>
                            {t("numberUnqualifiedThisMonth")}
                        </Text>
                    </View>
                    <View>
                        <Title>+{data?.by_month?.not_leave || 0}</Title>
                    </View>
                </View>
            </View>

            <Modal
                isVisible={visible}
                backdropOpacity={0.5}
                style={{ justifyContent: "flex-end", margin: 0 }}
                onBackdropPress={() => setVisible(false)}
                onBackButtonPress={() => setVisible(false)}
            >
                <View style={styles.modalContainer}>
                    <Title>{t("filter")}</Title>

                    <PickerSelect
                        control={control}
                        formState={formState}
                        placeholder=""
                        name="countries"
                        datas={countries?.length ? countries : []}
                        label={t("country")}
                        selectedId={factory?.country_id || selectCountry}
                        disabled={factory?.country_id}
                        handleChange={(id) => {
                            setSelectCountry(id);
                        }}
                    />

                    <PickerSelect
                        control={control}
                        formState={formState}
                        placeholder=""
                        name="company"
                        datas={filterFactor?.length ? filterFactor : []}
                        label={t("company")}
                        selectedId={factory?.factory_id || selectCompany}
                        disabled={factory?.factory_id}
                        handleChange={(id) => {
                            setSelectCompany(id);
                        }}
                    />

                    <PickerSelect
                        control={control}
                        formState={formState}
                        placeholder=""
                        name="branch"
                        datas={filterBranch?.length ? filterBranch : []}
                        label={t("branch")}
                        selectedId={selectBranch}
                        handleChange={(id) => {
                            setSelectBranch(id);
                        }}
                    />

                    <View
                        style={{
                            flexDirection: "row",
                            width: "100%",
                            paddingVertical: 20,
                            justifyContent: "space-between",
                        }}
                    >
                        <Button
                            mode="contained"
                            theme={{
                                colors: { primary: Theme.Colors.whiteColor },
                            }}
                            contentStyle={{ height: 50 }}
                            style={{ width: "45%" }}
                            onPress={() => {
                                setVisible(false);
                                handleRefetch();
                            }}
                        >
                            {t("reset")}
                        </Button>
                        <Button
                            mode="contained"
                            theme={{
                                colors: { primary: Theme.Colors.primary },
                            }}
                            contentStyle={{ height: 50 }}
                            style={{ width: "45%" }}
                            onPress={() => {
                                setParams({
                                    selectCountry: selectCountry,
                                    selectCompany: selectCompany,
                                    selectBranch: selectBranch,
                                });
                                setVisible(false);
                            }}
                        >
                            {t("filter")}
                        </Button>
                    </View>
                </View>
            </Modal>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    scrollContainer: {
        flexGrow: 1,
        padding: 10,
        backgroundColor: Theme.Colors.lightBackground,
    },
    container: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    cardFullContainer: {
        borderRadius: 10,
        width: width - 20,
        height: 130,
        marginBottom: 10,
        paddingVertical: 30,
    },
    cardContainer: {
        borderRadius: 10,
        width: width / 2 - 15,
        height: 130,
        marginBottom: 10,
        paddingVertical: 30,
    },
    workerContainer: {
        flexDirection: "row",
        marginLeft: 20,
    },
    listCardcontainer: {
        backgroundColor: Theme.Colors.whiteColor,
        padding: 10,
        paddingHorizontal: 15,
        borderRadius: 18,
        borderColor: Theme.Colors.borderColor,
        marginBottom: 10,
    },
    childCardContainer: {
        flexDirection: "row",
        paddingTop: 10,
        borderBottomWidth: 1,
        borderColor: Theme.Colors.borderColor,
        justifyContent: "space-between",
    },
    modalContainer: {
        flex: 1,
        alignItems: "center",
        height: height,
        padding: 16,
        backgroundColor: Theme.Colors.lightBackground,
    },
    modalContainerButton: {
        backgroundColor: Theme.Colors.borderColor,
        flexDirection: "row",
        width: "95%",
        borderRadius: 10,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
    },
});

export default Home;
