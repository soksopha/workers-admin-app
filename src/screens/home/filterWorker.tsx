import React, { useState, useLayoutEffect, useEffect } from "react";
import Modal from "react-native-modal";
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Text, Button, Title } from "react-native-paper";
import {
    StyleSheet,
    FlatList,
    View,
    Dimensions,
    TouchableOpacity,
} from "react-native";
import {
    useFilterList,
    useFactorList,
    useBranchList,
} from "@src/hooks/useFilters";
import {
    ModalSpinner,
    Spinner,
    ListItems,
    PickerSelect,
} from "@src/components";
import { useStorage } from "@src/hooks/useLocalStorage";
import { useFetchWorker } from "@src/hooks/useWorker";
import { Theme } from "@src/config/theme";
import { AccessKey } from "@src/utils/enums";

const { height } = Dimensions.get("window");
const FilterWorker: React.FC = ({ navigation, route }: any) => {
    const { t } = useTranslation();
    const { title, summaryText, processStatus, totalSummary } = route.params;
    const blackLists = [
        {
            id: 0,
            name_kh: t("blackList"),
            name_en: t("blackList"),
        },
        {
            id: 1,
            name_kh: t("normal"),
            name_en: t("normal"),
        },
    ];
    const { data: factory } = useStorage(AccessKey.FACTORY);
    const { handleSubmit, reset, control, formState } = useForm({
        defaultValues: {
            company: factory?.country_id,
            branch: factory?.factory_id,
        },
    });
    const [visible, setVisible] = useState<boolean>(false);
    const [searchQuery, setSearchQuery] = useState<any>({
        search: "",
        agency_id: 0,
        country_id: 0,
        factory_id: 0,
        branch_id: 0,
        status: "",
        process_status: processStatus,
    });
    const [handleSearchQuery, setHandleSearchQuery] = useState("");
    const [countTotalSummary, setCountTotalSummary] = useState<number>(
        totalSummary || 0
    );
    const [selectAgency, setSelectAgency] = useState<number>(0);
    const [selectCountry, setSelectCountry] = useState<number>(0);
    const [selectCompany, setSelectCompany] = useState<number>(0);
    const [selectNote, setSelectNote] = useState<number>(0);
    const [selectBranch, setSelectBranch] = useState<number>(0);
    const { data: filterLists } = useFilterList();
    const { data: filterFactor } = useFactorList(
        Number(factory?.country_id || selectCountry)
    );
    const { data: filterBranch } = useBranchList(
        Number(factory?.factory_id || selectCompany)
    );
    const {
        data,
        isLoading,
        fetchNextPage,
        isFetchingNextPage,
        refetch,
        isFetching,
    } = useFetchWorker(
        {
            search: "",
            agency_id: searchQuery?.agency_id,
            country_id: searchQuery?.country_id,
            factory_id: searchQuery?.factory_id,
            branch_id: searchQuery?.branch_id,
            status: "",
            process_status: processStatus,
        },
        true
    );

    const handleFilter = () => {
        setSearchQuery({
            search: "",
            agency_id: selectAgency,
            country_id: selectCountry,
            factory_id: selectCompany,
            branch_id: selectBranch,
            status: "",
            process_status: processStatus,
        });
        setVisible(false);
    };

    const handleRefetch = () => {
        reset();
        setSelectAgency(0);
        setSelectCountry(0);
        setSelectCompany(0);
        setSelectNote(0);
        setSelectBranch(0);
        setSearchQuery({
            search: "",
            agency_id: 0,
            country_id: 0,
            factory_id: 0,
            branch_id: 0,
            status: "",
            process_status: processStatus,
        });
        refetch();
    };

    useEffect(() => {
        setCountTotalSummary(data?.pages?.[0]?.count || 0);
    }, [data]);

    useLayoutEffect(() => {
        navigation.setOptions({
            title: title,
            headerShown: true,
        });
    }, [navigation]);
    const { agencies, countries } = filterLists || {};

    const renderItem = ({ item }: any) => {
        return <ListItems item={item} navigation={navigation} />;
    };

    return (
        <View style={styles.container}>
            <View style={styles.searchContainer}>
                <ModalSpinner visible={isLoading} />
                <View style={{ flex: 1, justifyContent: "center" }}>
                    <View
                        style={{
                            borderRadius: 10,
                            width: "100%",
                            padding: 15,
                            backgroundColor: "#E1E6E8",
                        }}
                    >
                        <Text>{`${t("all")} ${countTotalSummary}`}</Text>
                    </View>

                    <TouchableOpacity style={styles.searchICon}>
                        <TouchableOpacity
                            style={{
                                flexDirection: "row",
                                justifyContent: "space-evenly",
                                alignItems: "center",
                            }}
                            onPress={() => {
                                setVisible(true);
                            }}
                        >
                            <Icon
                                name="filter"
                                color={Theme.Colors.whiteColor}
                                size={15}
                            />
                            <Text style={{ color: Theme.Colors.whiteColor }}>
                                {t("filter")}
                            </Text>
                        </TouchableOpacity>
                    </TouchableOpacity>
                </View>
            </View>

            <FlatList
                ListEmptyComponent={
                    <Text style={{ textAlign: "center" }}>{t("no_data")}</Text>
                }
                contentContainerStyle={{
                    flexGrow: 1,
                    marginTop: 15,
                    marginBottom: 15,
                }}
                data={data?.pages.flatMap((page) => page.data) || []}
                keyExtractor={(_, index) => index.toString()}
                renderItem={renderItem}
                refreshing={isFetching}
                onRefresh={handleRefetch}
                onEndReachedThreshold={0.1}
                onEndReached={() => {
                    if (!isFetchingNextPage) {
                        fetchNextPage();
                    }
                }}
                ListFooterComponent={isFetchingNextPage ? <Spinner /> : null}
            />

            <Modal
                isVisible={visible}
                backdropOpacity={0.5}
                style={{ justifyContent: "flex-end", margin: 0 }}
                onBackdropPress={() => setVisible(false)}
                onBackButtonPress={() => setVisible(false)}
            >
                <View style={styles.modalContainer}>
                    <Title>{t("filter")}</Title>
                    <PickerSelect
                        control={control}
                        formState={formState}
                        placeholder=""
                        name="agencies"
                        datas={agencies || []}
                        label={t("agency")}
                        selectedId={selectAgency}
                        handleChange={(id) => {
                            setSelectAgency(id);
                        }}
                    />

                    <PickerSelect
                        control={control}
                        formState={formState}
                        placeholder=""
                        name="countries"
                        datas={countries || []}
                        label={t("country")}
                        selectedId={factory?.country_id || selectCountry}
                        disabled={factory?.country_id}
                        handleChange={(id) => {
                            setSelectCountry(id);
                        }}
                    />

                    <PickerSelect
                        control={control}
                        formState={formState}
                        placeholder=""
                        name="company"
                        datas={filterFactor || []}
                        label={t("company")}
                        selectedId={factory?.factory_id || selectCompany}
                        disabled={factory?.factory_id}
                        handleChange={(id) => {
                            setSelectCompany(id);
                        }}
                    />

                    <PickerSelect
                        control={control}
                        formState={formState}
                        placeholder=""
                        name="branch"
                        datas={filterBranch || []}
                        label={t("branch")}
                        selectedId={selectBranch}
                        handleChange={(id) => {
                            setSelectBranch(id);
                        }}
                    />

                    <PickerSelect
                        control={control}
                        formState={formState}
                        placeholder=""
                        name="note"
                        datas={blackLists}
                        label={t("note")}
                        selectedId={selectNote}
                        handleChange={(id) => {
                            setSelectNote(id);
                        }}
                    />
                    <View
                        style={{
                            flexDirection: "row",
                            width: "100%",
                            paddingVertical: 20,
                            justifyContent: "space-between",
                        }}
                    >
                        <Button
                            mode="contained"
                            theme={{
                                colors: { primary: Theme.Colors.whiteColor },
                            }}
                            contentStyle={{ height: 50 }}
                            style={{ width: "45%" }}
                            onPress={() => {
                                setVisible(false);
                                handleRefetch();
                            }}
                        >
                            {t("reset")}
                        </Button>
                        <Button
                            mode="contained"
                            theme={{
                                colors: { primary: Theme.Colors.primary },
                            }}
                            contentStyle={{ height: 50 }}
                            style={{ width: "45%" }}
                            onPress={handleFilter}
                        >
                            {t("filter")}
                        </Button>
                    </View>
                </View>
            </Modal>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        paddingVertical: 20,
        paddingHorizontal: 20,
        flex: 1,
        justifyContent: "space-between",
        backgroundColor: Theme.Colors.lightBackground,
    },
    searchContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginLeft: 5,
    },
    searchICon: {
        position: "absolute",
        backgroundColor: Theme.Colors.primary,
        borderRadius: 5,
        justifyContent: "center",
        width: 60,
        height: 30,
        right: 10,
    },
    modalContainer: {
        flex: 1,
        alignItems: "center",
        height: height,
        padding: 16,
        backgroundColor: Theme.Colors.lightBackground,
    },
    modalContainerButton: {
        backgroundColor: Theme.Colors.borderColor,
        flexDirection: "row",
        width: "95%",
        borderRadius: 10,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
    },
});

export default FilterWorker;
