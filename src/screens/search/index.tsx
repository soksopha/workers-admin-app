import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import { Searchbar, Text } from "react-native-paper";
import { View, StyleSheet, TouchableOpacity, FlatList } from "react-native";
import { ModalSpinner, ListItems, Spinner } from "@src/components";
import { useFetchWorker } from "@src/hooks/useWorker";
import { Theme } from "@src/config/theme";

const Search: React.FC = ({ navigation }: any) => {
  const { t } = useTranslation();
  const [enabled, setEnabled] = useState<boolean>(false);
  const [searchQuery, setSearchQuery] = useState("");
  const [handleSearchQuery, setHandleSearchQuery] = useState("");
  const {
    data,
    isLoading,
    refetch,
    fetchNextPage,
    isFetchingNextPage,
    isFetching,
  } = useFetchWorker(
    {
      process_status: 0,
      search: handleSearchQuery,
    },
    enabled,
  );

  const onChangeSearch = (query: any) => setSearchQuery(query);

  const renderItem = ({ item }: any) => {
    return <ListItems item={item} navigation={navigation} />;
  };

  return (
    <View style={styles.container}>
      <View style={styles.searchContainer}>
        <ModalSpinner visible={isLoading} />
        <Searchbar
          placeholder={t("searchWorker")}
          mode="bar"
          style={{ borderRadius: 20, width: "85%", backgroundColor: "#E8EDF0" }}
          onChangeText={onChangeSearch}
          onClearIconPress={() => setHandleSearchQuery("")}
          value={searchQuery}
        />
        <TouchableOpacity
          style={styles.searchContainer}
          onPress={() => {
            setHandleSearchQuery(searchQuery);
            setEnabled(true);
          }}
        >
          <Text style={styles.searchButton}>{t("search")}</Text>
        </TouchableOpacity>
      </View>

      <FlatList
        ListEmptyComponent={
          <Text style={{ textAlign: "center" }}>{t("no_data")}</Text>
        }
        contentContainerStyle={{
          flexGrow: 1,
          marginTop: 15,
          paddingHorizontal: 10,
        }}
        data={data?.pages.flatMap((page) => page.data) || []}
        keyExtractor={(_: any, index: { toString: () => any }) =>
          index.toString()
        }
        renderItem={renderItem}
        refreshing={isFetching}
        onRefresh={() => {
          setEnabled(false);
          refetch();
        }}
        onEndReachedThreshold={0.1}
        onEndReached={() => {
          if (!isFetchingNextPage) {
            fetchNextPage();
          }
        }}
        ListFooterComponent={isFetchingNextPage ? <Spinner /> : null}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    backgroundColor: Theme.Colors.lightBackground,
  },
  searchContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 5,
    marginLeft: 5,
  },
  searchHistory: {
    padding: 10,
  },
  searchButton: {
    fontSize: 14,
    color: Theme.Colors.blackColor,
  },
  histortyTitle: {
    fontSize: 14,
  },
  searchResultBadget: {
    borderWidth: 1,
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 20,
    marginRight: 10,
    borderColor: Theme.Colors.borderColor,
  },
  searchResultBadgetText: {
    color: Theme.Colors.blackColor,
    textAlign: "center",
  },
  removeButton: {
    backgroundColor: Theme.Colors.whiteColor,
    width: 30,
    height: 30,
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  removeContainer: {
    flexDirection: "row",
    paddingVertical: 5,
    justifyContent: "space-between",
  },
  titleText: {
    color: Theme.Colors.blackColor,
    lineHeight: 25,
  },
  descText: {
    color: Theme.Colors.darkColor,
    lineHeight: 25,
  },
});

export default Search;
