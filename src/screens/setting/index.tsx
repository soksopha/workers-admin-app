import React, { useState } from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import AsyncStorage from "@react-native-community/async-storage";
import { useTranslation } from "react-i18next";
import Icon from "react-native-vector-icons/Feather";
import { ScrollView, Image, View, StyleSheet } from "react-native";
import { Alert } from "@src/components";
import { Title, Text, Avatar, Divider, List } from "react-native-paper";
import { Logout } from "@src/utils/helpers";
import { resetNavigation, navigateTo } from "@src/navigation/Navigation";
import { Theme } from "@src/config/theme";
import { useStorage } from "@src/hooks/useLocalStorage";
import { AccessKey } from "@src/utils/enums";
import { APP_VERSION, IMAGE_PATH } from "@env";

const kmImg = require("@src/assets/images/flags/km.png");
const enImg = require("@src/assets/images/flags/uk.png");

const Setting: React.FC = ({ navigation }: any) => {
  const { t, i18n } = useTranslation();
  const queryClient = useQueryClient();
  const currentLanguage = i18n.language;
  const { data: profile } = useStorage(AccessKey.PROFILE);
  const [isVisible, setIsVisible] = useState(false);

  const toggleSwitch = () => {
    const newLanguage = currentLanguage === "km" ? "en" : "km";
    i18n.changeLanguage(newLanguage);
    AsyncStorage.setItem(AccessKey.LANGUAGE, JSON.stringify(newLanguage));
  };

  const handleLogout = () => {
    Logout();
    queryClient.clear();
    resetNavigation(navigation, "Login");
  };

  return (
    <ScrollView
      contentContainerStyle={styles.container}
      showsVerticalScrollIndicator={false}
    >
      <View>
        <Title style={styles.settingTitle}>{t("tabSetting")}</Title>
        <View style={styles.profileContainer}>
          <Avatar.Image
            size={80}
            style={{
              marginRight: 20,
              backgroundColor: Theme.Colors.borderColor,
            }}
            source={
              profile?.photo
                ? { uri: `${IMAGE_PATH}/${profile.photo}` }
                : require("@src/assets/images/avatar.png")
            }
          />
          <View>
            <Title style={{ marginBottom: -5 }}>{profile?.name || "N/A"}</Title>
            <Text>{profile?.email || "N/A"}</Text>
          </View>
        </View>
        <View
          style={{
            marginTop: 20,
            backgroundColor: Theme.Colors.lightBackground,
          }}
        >
          <List.Item
            title={t("profile")}
            titleStyle={styles.listTitle}
            style={styles.listContainer}
            left={(props) => <Icon name="user" size={20} />}
            right={(props) => <Icon name="chevron-right" size={20} />}
            onPress={() => navigateTo(navigation, "Profile")}
          />
          <Divider />
          <List.Item
            title={t("changePassword")}
            titleStyle={styles.listTitle}
            style={styles.listContainer}
            left={(props) => <Icon name="key" size={20} />}
            right={(props) => <Icon name="chevron-right" size={20} />}
            onPress={() => navigateTo(navigation, "ChangePassword")}
          />
          <Divider />
          <List.Item
            title={t("changeLanguage")}
            titleStyle={[styles.listTitle, { marginLeft: 2 }]}
            style={styles.listContainer}
            left={(props) => (
              <Image
                source={currentLanguage === "km" ? enImg : kmImg}
                style={styles.image}
                resizeMode="contain"
              />
            )}
            onPress={toggleSwitch}
          />
          <Divider />
          <List.Item
            title={t("logOut")}
            titleStyle={styles.listTitle}
            style={styles.listContainer}
            left={(props) => <Icon name="log-out" size={20} />}
            onPress={() => setIsVisible(true)}
          />
        </View>

        <Alert
          visible={isVisible}
          onDismiss={() => setIsVisible(false)}
          onAction={handleLogout}
          title={t("logout_message_title")}
          content={t("logout_message")}
        />
      </View>
      <View>
        <Text style={{ color: Theme.Colors.darkColor, textAlign: "center" }}>
          Version {APP_VERSION}
        </Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: Theme.Colors.lightBackground,
    flex: 1,
    justifyContent: "space-between",
  },
  settingTitle: {
    fontSize: 18,
    textAlign: "center",
    marginBottom: 10,
  },
  profileContainer: {
    paddingVertical: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  listContainer: {
    paddingVertical: 15,
    backgroundColor: Theme.Colors.lightBackground,
  },
  listTitle: {
    fontSize: 14,
    marginTop: -7,
    marginLeft: 10,
  },
  image: {
    width: 30,
    height: 25,
  },
});

export default Setting;
