import React, { useLayoutEffect } from "react";
import Icon from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import { useTranslation } from "react-i18next";
import { View, Dimensions, StyleSheet, TouchableOpacity } from "react-native";
import { Avatar, Title, Text } from "react-native-paper";
import { useStorage } from "@src/hooks/useLocalStorage";
import { Theme } from "@src/config/theme";
import { AccessKey } from "@src/utils/enums";
import { navigateTo } from "@src/navigation/Navigation";

const { width } = Dimensions.get("window");
const Profile: React.FC = ({ navigation }: any) => {
  const { t } = useTranslation();
  const { data: profile } = useStorage(AccessKey.PROFILE);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: t("profile"),
      headerShown: true,
      headerRight: () => (
        <TouchableOpacity
          style={{
            backgroundColor: Theme.Colors.whiteColor,
            padding: 10,
            borderRadius: 20,
          }}
          onPress={() => navigateTo(navigation, "ChangeProfile")}
        >
          <Icon name="edit" color={Theme.Colors.blackColor} size={18} />
        </TouchableOpacity>
      ),
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <View style={styles.center}>
        <Avatar.Image
          size={100}
          style={{
            marginRight: 20,
            marginBottom: 20,
            backgroundColor: Theme.Colors.borderColor,
          }}
          source={require("@src/assets/images/avatar.png")}
        />
      </View>
      <View style={styles.mainCard}>
        <TouchableOpacity
          onPress={() => navigateTo(navigation, "ChangeProfile")}
          style={styles.cardContainer}
        >
          <Icon
            style={styles.textCenter}
            name="user"
            color={Theme.Colors.blackColor}
            size={25}
          />
          <Text style={[styles.textCenter, { marginBottom: 5 }]}>
            {t("userName")}
          </Text>
          <Title style={styles.textCenter}>{profile?.name}</Title>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigateTo(navigation, "ChangeProfile")}
          style={styles.cardContainer}
        >
          <Icon
            style={styles.textCenter}
            name="user"
            color={Theme.Colors.blackColor}
            size={25}
          />
          <Text style={[styles.textCenter, { marginBottom: 5 }]}>
            {t("email")}
          </Text>
          <Title style={styles.textCenter}>{profile?.email}</Title>
        </TouchableOpacity>
      </View>
      <View style={styles.mainCard}>
        <TouchableOpacity
          onPress={() => navigateTo(navigation, "ChangeProfile")}
          style={styles.cardContainer}
        >
          <FontAwesome
            style={styles.textCenter}
            name="mars"
            color={Theme.Colors.blackColor}
            size={23}
          />
          <Text style={[styles.textCenter, { marginBottom: 5 }]}>
            {t("gender")}
          </Text>
          <Title style={styles.textCenter}>
            {profile?.sex == 1 ? t("male") : t("female")}
          </Title>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigateTo(navigation, "ChangeProfile")}
          style={styles.cardContainer}
        >
          <Icon
            style={styles.textCenter}
            name="gift"
            color={Theme.Colors.blackColor}
            size={25}
          />
          <Text style={[styles.textCenter, { marginBottom: 5 }]}>
            {t("dob")}
          </Text>
          <Title style={styles.textCenter}>{profile?.dob || "--"}</Title>
        </TouchableOpacity>
      </View>
      <View style={styles.mainCard}>
        <TouchableOpacity
          onPress={() => navigateTo(navigation, "ChangeProfile")}
          style={styles.addressContainer}
        >
          <Icon
            style={styles.textCenter}
            name="map-pin"
            color={Theme.Colors.blackColor}
            size={25}
          />
          <Text style={[styles.textCenter, { marginBottom: 5 }]}>
            {t("address")}
          </Text>
          <Title style={styles.textCenter}>{profile?.address || "--"}</Title>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    backgroundColor: Theme.Colors.lightBackground,
  },
  center: {
    alignItems: "center",
  },
  mainCard: {
    paddingHorizontal: 20,
    flexDirection: "row",
  },
  cardContainer: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Theme.Colors.placeHolderColor,
    width: width / 2 - 40,
    marginRight: 20,
    marginBottom: 20,
  },
  addressContainer: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: Theme.Colors.placeHolderColor,
    width: width - 60,
    marginRight: 20,
    marginBottom: 20,
  },
  textCenter: {
    textAlign: "center",
    fontSize: 14,
  },
});

export default Profile;
