import React, { useLayoutEffect } from "react";
import AsyncStorage from "@react-native-community/async-storage";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import { View, StyleSheet, Alert, ScrollView } from "react-native";
import { Button } from "react-native-paper";
import {
  ControlledTextInput,
  DateOfBirthInput,
  GenderRadio,
} from "@src/components";
import { useStorage } from "@src/hooks/useLocalStorage";
import { Theme } from "@src/config/theme";
import { AccessKey } from "@src/utils/enums";
import { changeProfile } from "@src/hooks/useCredential";
import { navigateToBack } from "@src/navigation/Navigation";

const ChangeProfile: React.FC = ({ navigation }: any) => {
  const { t } = useTranslation();
  const queryClient = useQueryClient();
  const { data: profile } = useStorage(AccessKey.PROFILE);

  const { handleSubmit, watch, control, formState } = useForm({
    defaultValues: {
      name: profile?.name,
      dob: profile?.dob,
      phone_number: profile?.phone_number,
      address: profile?.address,
      sex: `${profile?.sex}`,
    },
  });

  useLayoutEffect(() => {
    navigation.setOptions({
      title: t("profile"),
      headerShown: true,
    });
  }, [navigation]);

  const mutation = useMutation({
    mutationFn: changeProfile,
    onSuccess: (data: any) => {
      if (data) {
        navigateToBack(navigation);
        AsyncStorage.setItem(
          AccessKey.PROFILE,
          JSON.stringify({
            ...data,
            phone_number: watch("phone_number"),
          }),
        );
        queryClient.invalidateQueries({ queryKey: ["user-storage"] });
      }
    },
    onError: (error, variables, context) => {
      Alert.alert("Some thing went wrong");
    },
  });

  const onSubmit = (data: any) => {
    const { name, dob, sex, phone_number, address } = data;
    mutation.mutate({ name, dob, sex, phone_number, address, id: profile?.id });
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View>
        <ControlledTextInput
          control={control}
          formState={formState}
          name="name"
          label={t("fullName")}
          rules={{ required: t("fullName") }}
          style={styles.input}
        />

        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View style={{ width: "55%" }}>
            <DateOfBirthInput
              control={control}
              formState={formState}
              name="dob"
              label={t("dob")}
              rules={{ required: t("dob") }}
              style={styles.input}
            />
          </View>
          <View style={{ width: "45%", alignItems: "flex-end" }}>
            <GenderRadio
              control={control}
              formState={formState}
              name="sex"
              label={t("gender")}
              rules={{ required: false }}
            />
          </View>
        </View>

        <ControlledTextInput
          control={control}
          formState={formState}
          name="phone_number"
          label={t("phoneNumber")}
          rules={{ required: t("phoneNumber") }}
          style={styles.input}
        />

        <ControlledTextInput
          control={control}
          formState={formState}
          name="address"
          label={t("address")}
          rules={{ required: t("address") }}
          style={styles.input}
        />
      </View>
      <View>
        <Button
          mode="contained"
          loading={mutation.isPending}
          disabled={mutation.isPending}
          theme={{ colors: { primary: Theme.Colors.primary } }}
          contentStyle={{ height: 50 }}
          style={styles.button}
          onPress={handleSubmit(onSubmit)}
        >
          {t("save")}
        </Button>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: Theme.Colors.lightBackground,
  },
  input: {
    marginBottom: 16,
  },
  button: {
    width: "100%",
  },
});

export default ChangeProfile;
