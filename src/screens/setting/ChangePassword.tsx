import React, { useLayoutEffect } from "react";
import { useMutation } from "@tanstack/react-query";
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import { View, Alert, StyleSheet, ScrollView } from "react-native";
import { Button } from "react-native-paper";
import { ControlledTextInput } from "@src/components";
import { useStorage } from "@src/hooks/useLocalStorage";
import { changePassword } from "@src/hooks/useCredential";
import { AccessKey } from "@src/utils/enums";
import { navigateToBack } from "@src/navigation/Navigation";
import { Theme } from "@src/config/theme";

const Profile: React.FC = ({ navigation }: any) => {
  const { t } = useTranslation();
  const { handleSubmit, control, setError, formState } = useForm();
  const { data: profile } = useStorage(AccessKey.PROFILE);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: t("changePassword"),
      headerShown: true,
    });
  }, [navigation]);

  const mutation = useMutation({
    mutationFn: changePassword,
    onSuccess: (data: any) => {
      if (data) {
        navigateToBack(navigation);
      }
    },
    onError: (error, variables, context) => {
      Alert.alert("Some thing went wrong");
    },
  });

  const onSubmit = (data: any) => {
    const { password, new_password, confirmNewPassword } = data;
    if (new_password !== confirmNewPassword) {
      setError("new_password", {
        type: "custom",
        message: t("passwordNotMatch"),
      });
      setError("confirmNewPassword", {
        type: "custom",
        message: t("passwordNotMatch"),
      });
    }
    mutation.mutate({ password, new_password, username: profile?.username });
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View>
        <ControlledTextInput
          secureTextEntry
          control={control}
          formState={formState}
          name="password"
          label={t("currentPassword")}
          rules={{
            required: t("required"),
            minLength: {
              value: 8,
              message: t("passwordMinLengthMessage"),
            },
          }}
          style={styles.input}
        />

        <ControlledTextInput
          secureTextEntry
          control={control}
          formState={formState}
          name="new_password"
          label={t("newPassword")}
          rules={{
            required: t("required"),
            minLength: {
              value: 8,
              message: t("passwordMinLengthMessage"),
            },
          }}
          style={styles.input}
        />

        <ControlledTextInput
          secureTextEntry
          control={control}
          formState={formState}
          name="confirmNewPassword"
          label={t("confirmNewPassword")}
          rules={{
            required: t("required"),
            minLength: {
              value: 8,
              message: t("passwordMinLengthMessage"),
            },
          }}
          style={styles.input}
        />
      </View>

      <Button
        mode="contained"
        loading={mutation.isPending}
        disabled={mutation.isPending}
        theme={{ colors: { primary: Theme.Colors.primary } }}
        contentStyle={{ height: 50 }}
        style={styles.button}
        onPress={handleSubmit(onSubmit)}
      >
        {t("save")}
      </Button>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: Theme.Colors.lightBackground,
  },
  input: {
    marginBottom: 16,
  },
  button: {
    width: "100%",
  },
});

export default Profile;
