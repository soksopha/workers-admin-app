import React, { useLayoutEffect, useState } from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import Modal from "react-native-modal";
import Icon from "react-native-vector-icons/FontAwesome";
import Feather from "react-native-vector-icons/Feather";
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import {
    View,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    RefreshControl,
    Alert,
} from "react-native";
import { Card, Avatar, Button, Text } from "react-native-paper";
import { useFetchWorkerDetail } from "@src/hooks/useWorker";
import { removeBlackList, takeOut } from "@src/hooks/useBlackList";
import { useStorage } from "@src/hooks/useLocalStorage";
import { validateCredential } from "@src/hooks/useCredential";
import AlertMessage, { DialogMessage } from "@src/components/AlertMessage";
import { ModalSpinner, ControlledTextInput } from "@src/components";
import { Theme } from "@src/config/theme";
import { AccessKey } from "@src/utils/enums";
import { navigateTo } from "@src/navigation/Navigation";
import { IMAGE_PATH } from "@env";

const HistoryWorker: React.FC = ({ navigation, route }: any) => {
    const { t } = useTranslation();
    const queryClient = useQueryClient();
    const { id } = route.params;
    const [confirmPasswordVisible, setConfirmPasswordVisible] = useState<any>({
        visible: false,
        type: 0,
    });
    const [visibleSelected, setVisibleSelected] = useState<boolean>(false);
    const [visible, setVisible] = useState<boolean>(false);
    const { data, isLoading, refetch, isFetching, error } =
        useFetchWorkerDetail(id);
    const { data: profile } = useStorage(AccessKey.PROFILE);
    const { handleSubmit, control, setError, reset, formState } = useForm();
    const notAllowRoleId = profile?.role_id != 7;
    const preparedDocument: boolean =
        data?.status === "រៀបចំឯកសារ (Prepare document)";

    useLayoutEffect(() => {
        navigation.setOptions({
            title: t("historyWorker"),
            headerShown: true,
            contentStyle: {
                paddingTop: 0,
            },
        });
    }, [navigation]);

    const refetchQueryKey = () => {
        refetch();
        setConfirmPasswordVisible({
            visible: false,
            type: 0,
        });
        setVisible(false);
        queryClient.refetchQueries({ queryKey: ["search-workers"] });
        reset();
    };

    const mutation = useMutation({
        mutationFn: validateCredential,
        onSuccess: async (response: any) => {
            if (response?.status) {
                reset();
                if (confirmPasswordVisible.type == 1) {
                    if (data?.is_black_list == "1") {
                        const form = new FormData();
                        form.append("worker_id", data?.id);
                        form.append("status", "0");
                        form.append("type", "0");
                        form.append("factory_id", "0");
                        form.append("country_id", "0");
                        form.append("start_date", '""');
                        form.append("duration", '""');
                        form.append("reason", '""');
                        addBlackList.mutate(form);
                    } else {
                        setConfirmPasswordVisible({
                            visible: false,
                            type: 0,
                        });
                        setVisible(false);
                        navigateTo(navigation, "EditBlackList", {
                            workerId: data?.id,
                        });
                    }
                } else if (confirmPasswordVisible?.type === 0) {
                    setConfirmPasswordVisible({
                        visible: false,
                        type: 0,
                    });
                    setVisible(false);
                    navigateTo(navigation, "EditWorker", {
                        workers: data,
                    });
                    reset();
                } else {
                    setVisibleSelected(true);
                    setConfirmPasswordVisible(false);
                }
            } else {
                setError("password", {
                    type: "custom",
                    message: t("passwordInvalid"),
                });
            }
        },
        onError: (error, variables, context) => {
            Alert.alert(t("invalidUserNamePassword"));
        },
    });

    const addBlackList = useMutation({
        mutationFn: removeBlackList,
        onSuccess: async (data: any) => {
            if (data) {
                refetchQueryKey();
            }
        },
        onError: (error, variables, context) => {
            Alert.alert("Something went wrong");
        },
    });

    const takeOutWorker = useMutation({
        mutationFn: takeOut,
        onSuccess: async (data: any) => {
            if (data) {
                refetchQueryKey();
                setVisibleSelected(false);
            }
        },
        onError: (error, variables, context) => {
            Alert.alert("Something went wrong");
        },
    });

    const onSubmit = (data: any) => {
        const { password } = data;
        mutation.mutate({ username: profile?.username, password });
    };

    const onSubmitRemoveSelect = (formData: any) => {
        const { remove_reason } = formData;
        takeOutWorker.mutate({
            worker_id: data?.id,
            remove_reason: remove_reason,
        });
    };

    if (
        isLoading ||
        addBlackList.isPending ||
        mutation.isPending ||
        takeOutWorker.isPending
    ) {
        return <ModalSpinner visible />;
    }

    return (
        <AlertMessage>
            <ScrollView
                contentContainerStyle={styles.container}
                refreshControl={
                    <RefreshControl
                        refreshing={isFetching}
                        onRefresh={() => refetch()}
                    />
                }
            >
                <DialogMessage mutation={takeOutWorker} />
                <View style={{ alignItems: "center", marginBottom: 20 }}>
                    <View>
                        <Avatar.Image
                            size={100}
                            style={{
                                marginBottom: 20,
                                backgroundColor: Theme.Colors.borderColor,
                            }}
                            source={
                                data?.photo?.length > 0
                                    ? {
                                          uri: `${IMAGE_PATH}/${data.photo[0].name}`,
                                      }
                                    : require("@src/assets/images/avatar.png")
                            }
                        />
                        {notAllowRoleId ? (
                            <TouchableOpacity
                                style={styles.uploadImage}
                                onPress={() =>
                                    navigateTo(
                                        navigation,
                                        "UploadImageWorker",
                                        {
                                            workerId: data?.id || "",
                                            photos: data?.photo || [],
                                        }
                                    )
                                }
                            >
                                <Icon name="camera" size={12} />
                            </TouchableOpacity>
                        ) : null}
                    </View>

                    <Text style={styles.profileText}>{data?.name_kh}</Text>
                    <Text style={styles.profileTitleText}>{data?.name_en}</Text>
                    <View
                        style={[
                            styles.statusContainer,
                            { borderColor: data?.color, borderWidth: 1 },
                        ]}
                    >
                        <Icon color={data?.color} name="circle" />
                        <Text
                            style={[styles.statusText, { color: data?.color }]}
                        >
                            {data?.status}
                        </Text>
                    </View>
                </View>
                <Card>
                    <Card.Content>
                        <View
                            style={{
                                flexDirection: "row",
                                justifyContent: "space-between",
                            }}
                        >
                            <View style={{ width: "60%" }}>
                                <View style={styles.mainText}>
                                    <Text style={styles.titleText}>
                                        {t("dob")}
                                    </Text>
                                    <Text style={styles.descText}>
                                        {data?.dob}
                                    </Text>
                                </View>
                                <View style={styles.mainText}>
                                    <Text style={styles.titleText}>
                                        {t("gender")}
                                    </Text>
                                    <Text style={styles.descText}>
                                        {data?.sex == 1
                                            ? t("male")
                                            : t("female")}
                                    </Text>
                                </View>
                            </View>
                            <View style={{ width: "40%" }}>
                                <View style={styles.mainText}>
                                    <Text style={styles.titleText}>
                                        {t("idCard")}
                                    </Text>
                                    <Text style={styles.descText}>
                                        {data?.id_card || "N/A"}
                                    </Text>
                                </View>
                                <View style={styles.mainText}>
                                    <Text style={styles.titleText}>
                                        {t("phone")}
                                    </Text>
                                    <Text style={styles.descText}>
                                        {data?.phone_number || "N/A"}
                                    </Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ width: "100%", marginBottom: 5 }}>
                            <Text style={styles.titleText}>
                                {t("passport")}
                            </Text>
                            <Text style={styles.descText}>
                                {data?.passport_no || "N/A"}
                            </Text>
                        </View>
                        <View style={{ width: "100%", marginBottom: 5 }}>
                            <Text style={styles.titleText}>
                                {t("pobAddress")}
                            </Text>
                            <Text style={styles.descText}>
                                {data?.pob_provinces || "N/A"}
                            </Text>
                        </View>
                        <View style={{ width: "100%", marginBottom: 5 }}>
                            <Text style={styles.titleText}>
                                {t("currentAddress")}
                            </Text>
                            <Text style={styles.descText}>
                                {data?.address_kh || "N/A"}
                            </Text>
                        </View>
                        <View style={{ width: "100%", marginBottom: 5 }}>
                            <Text style={styles.descText}>
                                {data?.address_en || "N/A"}
                            </Text>
                        </View>
                    </Card.Content>
                </Card>

                {notAllowRoleId ? (
                    <View
                        style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                            marginTop: 20,
                        }}
                    >
                        <TouchableOpacity
                            style={styles.buttonViewDetail}
                            onPress={() =>
                                navigateTo(navigation, "DetailWorker", {
                                    id: data?.id,
                                })
                            }
                        >
                            <Feather
                                name="eye"
                                size={18}
                                color={Theme.Colors.greenColor}
                                style={{ marginRight: 5 }}
                            />
                            <Text style={styles.textViewDetail}>
                                {t("viewDetail")}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.buttonViewDetail}
                            onPress={() => setVisible(true)}
                        >
                            <Feather
                                name="more-horizontal"
                                size={18}
                                style={{ marginRight: 5 }}
                            />
                            <Text>{t("other")}</Text>
                        </TouchableOpacity>
                    </View>
                ) : null}

                <Modal
                    isVisible={confirmPasswordVisible?.visible}
                    backdropColor="transparent"
                    onBackdropPress={() => {
                        setConfirmPasswordVisible({
                            visible: false,
                            type: 0,
                        });
                    }}
                    onBackButtonPress={() => {
                        setConfirmPasswordVisible({
                            visible: false,
                            type: 0,
                        });
                    }}
                >
                    <View style={[styles.modalContainer, { height: 200 }]}>
                        <ControlledTextInput
                            secureTextEntry
                            control={control}
                            formState={formState}
                            name="password"
                            label={t("confirmPassword")}
                            rules={{
                                required: t("required"),
                                minLength: {
                                    value: 8,
                                    message: t("passwordMinLengthMessage"),
                                },
                            }}
                            style={styles.input}
                        />

                        <Button
                            mode="contained"
                            loading={mutation.isPending}
                            disabled={mutation.isPending}
                            theme={{
                                colors: { primary: Theme.Colors.primary },
                            }}
                            contentStyle={{ height: 50 }}
                            style={styles.button}
                            onPress={handleSubmit(onSubmit)}
                        >
                            {t("confirm")}
                        </Button>
                    </View>
                </Modal>

                <Modal
                    isVisible={visible}
                    backdropOpacity={0.5}
                    style={{ justifyContent: "flex-end", margin: 0 }}
                    onBackdropPress={() => setVisible(false)}
                    onBackButtonPress={() => setVisible(false)}
                >
                    <View
                        style={[
                            styles.modalContainer,
                            { height: preparedDocument ? 200 : 150 },
                        ]}
                    >
                        <TouchableOpacity
                            style={styles.modalContainerButton}
                            onPress={() =>
                                setConfirmPasswordVisible({
                                    visible: true,
                                    type: 0,
                                })
                            }
                        >
                            <Icon
                                name="edit"
                                color={Theme.Colors.blackColor}
                                size={18}
                            />
                            <Text style={{ marginLeft: 10 }}>{t("edit")}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            style={styles.modalContainerButton}
                            onPress={() =>
                                setConfirmPasswordVisible({
                                    visible: true,
                                    type: 1,
                                })
                            }
                        >
                            <Icon
                                name={
                                    data?.is_black_list == "1"
                                        ? "minus"
                                        : "plus"
                                }
                                color={Theme.Colors.redColor}
                                size={18}
                            />
                            <Text
                                style={{
                                    marginLeft: 10,
                                    color: Theme.Colors.redColor,
                                }}
                            >
                                {data?.is_black_list == "1"
                                    ? t("removeBlackList")
                                    : t("blackList")}
                            </Text>
                        </TouchableOpacity>
                        {preparedDocument ? (
                            <TouchableOpacity
                                style={styles.modalContainerButton}
                                onPress={() =>
                                    setConfirmPasswordVisible({
                                        visible: true,
                                        type: 2,
                                    })
                                }
                            >
                                <Icon
                                    name={"minus"}
                                    color={Theme.Colors.redColor}
                                    size={18}
                                />
                                <Text
                                    style={{
                                        marginLeft: 10,
                                        color: Theme.Colors.redColor,
                                    }}
                                >
                                    {t("removeFromSelect")}
                                </Text>
                            </TouchableOpacity>
                        ) : null}
                    </View>
                </Modal>

                <Modal
                    isVisible={visibleSelected}
                    backdropOpacity={0.5}
                    style={{ justifyContent: "flex-end", margin: 0 }}
                    onBackdropPress={() => setVisibleSelected(false)}
                    onBackButtonPress={() => setVisibleSelected(false)}
                >
                    <View style={styles.modalContainer}>
                        <ControlledTextInput
                            secureTextEntry
                            control={control}
                            formState={formState}
                            name="remove_reason"
                            label={t("reasonFromSelect")}
                            rules={{
                                required: t("required"),
                            }}
                            style={styles.inputTextArea}
                        />
                        <Button
                            mode="contained"
                            loading={mutation.isPending}
                            disabled={mutation.isPending}
                            theme={{
                                colors: { primary: Theme.Colors.primary },
                            }}
                            contentStyle={{ height: 50 }}
                            style={styles.button}
                            onPress={handleSubmit(onSubmitRemoveSelect)}
                        >
                            {t("ok")}
                        </Button>
                    </View>
                </Modal>
            </ScrollView>
        </AlertMessage>
    );
};

const styles = StyleSheet.create({
    container: {
        padding: 20,
        flexGrow: 1,
        backgroundColor: Theme.Colors.lightBackground,
    },
    profileText: {
        color: Theme.Colors.darkColor,
        fontSize: 17,
    },
    profileTitleText: {
        color: Theme.Colors.blackColor,
        fontSize: 16,
    },
    titleText: {
        color: Theme.Colors.darkColor,
        lineHeight: 25,
        fontSize: 14,
        marginBottom: 5,
    },
    descText: {
        color: Theme.Colors.blackColor,
        lineHeight: 25,
        fontSize: 15,
        marginBottom: 5,
    },
    statusContainer: {
        marginTop: 10,
        paddingHorizontal: 10,
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: Theme.Colors.whiteColor,
        minWidth: 105,
        justifyContent: "space-between",
        padding: 5,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: Theme.Colors.orangeColor,
    },
    statusText: {
        fontSize: 14,
        marginLeft: 10,
        color: Theme.Colors.orangeColor,
    },
    buttonViewDetail: {
        padding: 10,
        borderRadius: 10,
        width: "45%",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: Theme.Colors.whiteColor,
        flexDirection: "row",
    },
    textViewDetail: {
        color: Theme.Colors.greenColor,
    },
    modalContainer: {
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        // height: 200,
        padding: 16,
    },
    modalContainerButton: {
        backgroundColor: Theme.Colors.borderColor,
        flexDirection: "row",
        width: "95%",
        borderRadius: 10,
        padding: 10,
        marginBottom: 10,
        alignItems: "center",
    },
    input: {
        width: "100%",
        marginBottom: 16,
    },
    inputTextArea: {
        width: "100%",
        // height: 150,
        marginBottom: 16,
    },
    button: {
        width: "100%",
    },
    uploadImage: {
        position: "absolute",
        backgroundColor: Theme.Colors.whiteColor,
        borderRadius: 20,
        padding: 10,
        bottom: 20,
        right: 0,
    },
    mainText: {
        marginBottom: 5,
    },
});

export default HistoryWorker;
