import React, { useLayoutEffect, useState } from "react";
import { addMonths, format, parse } from "date-fns";
import { View, Alert, StyleSheet, ScrollView } from "react-native";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import { Button } from "react-native-paper";
import {
  ControlledTextInput,
  DateOfBirthInput,
  PickerSelect,
  CountryRadio,
} from "@src/components";
import { useFilterList, useFactorList } from "@src/hooks/useFilters";
import { blackList } from "@src/hooks/useBlackList";
import { navigateToBack } from "@src/navigation/Navigation";
import { Theme } from "@src/config/theme";

const EditBlackList: React.FC = ({ navigation, route }: any) => {
  const { t } = useTranslation();
  const { workerId } = route.params;
  const queryClient = useQueryClient();
  const { handleSubmit, setValue, getValues, watch, control, formState } =
    useForm({
      defaultValues: {
        type: "1",
        factory_id: 0,
        country_id: 0,
        start_date: "",
        duration: 0,
        endDate: `0 ${t("day")}`,
      },
    });

  const [selectCountry, setSelectCountry] = useState<number>(0);
  const [selectAgency, setSelectAgency] = useState<number>(0);

  const { data: filterLists } = useFilterList();
  const { countries } = filterLists || {};
  const { data: filterFactor } = useFactorList(Number(selectCountry));

  useLayoutEffect(() => {
    navigation.setOptions({
      title: t("blackList"),
      headerShown: true,
    });
  }, [navigation]);

  const addMonth = () => {
    const durationValue = getValues("duration");
    const duration = durationValue ? durationValue : 0;
    if (duration > 0 && getValues("start_date")) {
      const currentDate = parse(
        getValues("start_date"),
        "yyyy-MM-dd",
        new Date(),
      );
      const newDate = addMonths(currentDate, duration);
      setValue("endDate", format(newDate, "yyyy-MM-dd"));
    }
  };

  const mutation = useMutation({
    mutationFn: blackList,
    onSuccess: (data: any) => {
      if (data) {
        queryClient.invalidateQueries({ queryKey: ["workers-detail"] });
        queryClient.invalidateQueries({ queryKey: ["search-workers"] });
        navigateToBack(navigation);
      } else {
        Alert.alert("Something went wrong");
      }
    },
    onError: (error, variables, context) => {
      Alert.alert("Something went wrong");
    },
  });

  const onSubmit = (data: any) => {
    mutation.mutate({ ...data, worker_id: workerId, status: 1 });
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <CountryRadio
        control={control}
        formState={formState}
        name="type"
        rules={{ required: true }}
      />
      {watch("type") == "1" ? (
        <PickerSelect
          control={control}
          formState={formState}
          placeholder=""
          name="country_id"
          datas={countries || []}
          label={t("country")}
          selectedId={selectCountry}
          handleChange={(id) => {
            setSelectCountry(id);
          }}
        />
      ) : null}
      {watch("type") == "2" ? (
        <>
          <PickerSelect
            control={control}
            formState={formState}
            placeholder=""
            name="country_id"
            datas={countries || []}
            label={t("country")}
            rules={{ required: t("required") }}
            selectedId={selectCountry}
            handleChange={(id) => {
              setSelectCountry(id);
            }}
          />

          <PickerSelect
            control={control}
            formState={formState}
            placeholder=""
            name="factory_id"
            datas={filterFactor || []}
            label={t("factory")}
            rules={{ required: t("required") }}
            selectedId={selectAgency}
            handleChange={(id) => {
              setSelectAgency(id);
            }}
          />
        </>
      ) : null}
      <ControlledTextInput
        control={control}
        formState={formState}
        name="duration"
        label={t("durationBlackList")}
        rules={{ required: t("required") }}
        style={styles.input}
        onBlur={addMonth}
      />
      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        <View style={{ width: "48%" }}>
          <DateOfBirthInput
            control={control}
            formState={formState}
            name="start_date"
            label={t("blackListingDay")}
            rules={{ required: t("required") }}
            style={styles.input}
            onBlur={addMonth}
          />
        </View>
        <View style={{ width: "48%" }}>
          <ControlledTextInput
            control={control}
            disabled
            formState={formState}
            name="endDate"
            label={t("endDate")}
            rules={{ required: false }}
            style={styles.input}
          />
        </View>
      </View>
      <ControlledTextInput
        control={control}
        formState={formState}
        name="reason"
        label={t("reason")}
        rules={{ required: t("required") }}
        style={styles.input}
      />

      <View style={{ paddingTop: 30 }}>
        <Button
          mode="contained"
          loading={mutation.isPending}
          disabled={mutation.isPending}
          theme={{ colors: { primary: Theme.Colors.primary } }}
          contentStyle={{ height: 50 }}
          style={styles.button}
          onPress={handleSubmit(onSubmit)}
        >
          {t("save")}
        </Button>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    flexGrow: 1,
    backgroundColor: Theme.Colors.lightBackground,
  },
  input: {
    marginBottom: 16,
  },
  button: {
    width: "100%",
  },
});

export default EditBlackList;
