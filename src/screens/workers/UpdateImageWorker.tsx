import React, { useEffect, useLayoutEffect, useState } from "react";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import {
  View,
  PermissionsAndroid,
  Platform,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  Image,
  Alert,
  Dimensions,
} from "react-native";
import Modal from "react-native-modal";
import { Text } from "react-native-paper";
import ImageResizer from "react-native-image-resizer";
import Icon from "react-native-vector-icons/FontAwesome6";
import { scale, verticalScale } from "react-native-size-matters";
import { useTranslation } from "react-i18next";
import { uploadImage, dropImage } from "@src/hooks/useUpload";
import {
  launchCamera,
  launchImageLibrary,
  CameraOptions,
  Asset,
} from "react-native-image-picker";
import { ModalSpinner } from "@src/components";
import { Theme } from "@src/config/theme";
import { IMAGE_PATH } from "@env";

const android = Platform.OS === "android";
const { width, height } = Dimensions.get("window");

const UpdateImageWorker: React.FC = ({ navigation, route }: any) => {
  const { t } = useTranslation();
  const { workerId, photos } = route.params || {};
  const queryClient = useQueryClient();
  const [removeImg, setRemoveImg] = useState<any[]>([]);
  const [imageList, setImageList] = useState<any[]>([]);
  const [modalVisible, setModalVisible] = useState(false);
  const [imageToShow, setImageToShow] = useState<any>(null);

  const mutation = useMutation({
    mutationFn: uploadImage,
    onSuccess: (data: any) => {
      if (data) {
        const responseImg: any = {
          uri: `${IMAGE_PATH}/${data.image}`,
          id: data.worker_doc_id,
        };
        const newImageList = [...imageList, responseImg];
        setImageList(newImageList);
        queryClient.invalidateQueries({ queryKey: ["workers-detail"] });
      } else {
        Alert.alert(t("failUploadImg"));
      }
    },
    onError: (error, variables, context) => {
      Alert.alert(t("failUploadImg"));
    },
  });

  const mutationImg = useMutation({
    mutationFn: dropImage,
    onSuccess: (data: any) => {
      if (data) {
        const filteredImageList = imageList.filter(
          (img) => img.uri !== removeImg,
        );
        setImageList(filteredImageList);
        setModalVisible(false);
        queryClient.invalidateQueries({ queryKey: ["workers-detail"] });
      } else {
        Alert.alert(t("canNotDeleteImg"));
      }
    },
    onError: (error, variables, context) => {
      Alert.alert(t("canNotDeleteImg"));
    },
  });

  const uploadAttachmentFile = (fileName: any, responseFile: any) => {
    try {
      const form = new FormData();
      form.append("worker_id", workerId);
      form.append("image", {
        uri: responseFile.uri,
        name: fileName,
        type: "image/png",
      });
      mutation.mutate(form);
    } catch (error) {
      Alert.alert(t("failUploadImg"));
    }
  };

  useEffect(() => {
    if (photos?.length > 0) {
      const tempImg = photos.map((img: any) => ({
        uri: `${IMAGE_PATH}/${img.name}`,
        id: img.worker_doc_id,
      }));
      setImageList(tempImg);
    }
  }, [photos]);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: t("uploadImage"),
      headerShown: true,
      contentStyle: {
        paddingTop: 0,
      },
    });
  }, [navigation]);

  const resizeImage = (
    file: Asset | any,
    callBack: (fileName: string, response: any) => void,
  ) => {
    const rotation = 0;
    const screenWidth = Dimensions.get("window").width;
    const targetWidth = screenWidth;
    const targetHeight = Math.round((targetWidth / file.width) * file.height);
    ImageResizer.createResizedImage(
      file.uri,
      targetWidth,
      targetHeight,
      "PNG",
      100,
      rotation,
    ).then((response) => {
      callBack(file.fileName, response);
    });
  };

  const requestCameraPermission = async (): Promise<string> => {
    let granted: string = "";
    if (android) {
      granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
        {
          title: "Request Camera Permission",
          message: "Access Camera Permission",
          buttonNeutral: "Ask me later",
          buttonNegative: "Cancel",
          buttonPositive: "Ok",
        },
      );

      granted =
        granted === PermissionsAndroid.RESULTS.GRANTED ? "granted" : "denied";
    } else {
      granted = "granted";
    }

    return Promise.resolve(granted);
  };

  const handleOnLaunchCamera = async () => {
    if (imageList?.length === 9) {
      Alert.alert(t("allowImage"));
      return;
    }
    const response = await requestCameraPermission();

    if (response === "granted") {
      const options: CameraOptions = {
        presentationStyle: "fullScreen",
        mediaType: "photo",
      };

      launchCamera(options, (response) => {
        if (response && response.assets) {
          const newImageList = [...imageList, response.assets[0]];
          resizeImage(response.assets[0], uploadAttachmentFile);
        }
      });
    }
  };

  const handleOnSelectImage = async () => {
    if (imageList?.length === 9) {
      Alert.alert(t("allowImage"));
      return;
    }

    const options: CameraOptions = {
      mediaType: "photo",
    };

    launchImageLibrary(options, (response) => {
      if (response && response.assets) {
        const newImageList = [...imageList, response.assets[0]];
        // setImageList(newImageList);
        resizeImage(response.assets[0], uploadAttachmentFile);
      }
    });
  };

  const handleImagePress = (image: React.SetStateAction<null>) => {
    setImageToShow(image);
    setModalVisible(true);
  };

  const handleRemoveImage = (image: any) => {
    Alert.alert(
      t("removeImage"),
      t("removeImageDesc"),
      [
        {
          text: t("cancel"),
          style: "cancel",
        },
        {
          text: t("remove"),
          onPress: () => {
            setRemoveImg(image.uri);
            mutationImg.mutate({
              worker_doc_id: image?.id,
            });
          },
        },
      ],
      { cancelable: false },
    );
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const renderImageContainers = () => {
    return (
      <View style={styles.gridContainer}>
        {[0, 1, 2].map((rowIndex) => (
          <View key={rowIndex} style={styles.gridRow}>
            {[0, 1, 2].map((colIndex) => {
              const imageIndex = rowIndex * 3 + colIndex;
              const image = imageList[imageIndex];
              return (
                <TouchableOpacity
                  key={colIndex}
                  disabled={image ? false : true}
                  onPress={() => handleImagePress(image)}
                  onLongPress={() => handleRemoveImage(image)}
                  style={styles.imageContainer}
                >
                  {image ? (
                    <Image
                      source={{ uri: image.uri }}
                      style={styles.imageContainer}
                    />
                  ) : (
                    <Icon
                      name="image"
                      color={Theme.Colors.blackColor}
                      style={{ textAlign: "center" }}
                      size={30}
                    />
                  )}
                </TouchableOpacity>
              );
            })}
          </View>
        ))}
      </View>
    );
  };

  return (
    <ScrollView style={styles.container}>
      <ModalSpinner visible={mutation.isPending || mutationImg.isPending} />

      {renderImageContainers()}

      <Modal
        isVisible={modalVisible}
        onBackdropPress={closeModal}
        onBackButtonPress={closeModal}
      >
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          {imageToShow && (
            <Image
              source={{ uri: imageToShow.uri }}
              style={styles.viewImageContainer}
            />
          )}
        </View>
      </Modal>

      <View
        style={{
          marginTop: 10,
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: 20,
        }}
      >
        <TouchableOpacity
          style={styles.buttonImage}
          onPress={handleOnLaunchCamera}
        >
          <Icon
            color={Theme.Colors.blackColor}
            name="camera"
            style={{ marginRight: 10 }}
            size={20}
          />
          <Text>{t("takeImage")}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonImage}
          onPress={handleOnSelectImage}
        >
          <Icon
            color={Theme.Colors.blackColor}
            name="image"
            style={{ marginRight: 10 }}
            size={20}
          />
          <Text>{t("selectImage")}</Text>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: Theme.Colors.lightBackground,
  },
  gridContainer: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  gridRow: {
    flexDirection: "row",
    padding: 2,
    marginBottom: 10,
  },
  imageContainer: {
    backgroundColor: Theme.Colors.greyColor,
    height: verticalScale(100),
    width: scale(100),
    borderRadius: 10,
    marginRight: 10,
    justifyContent: "center",
    resizeMode: "center",
  },
  viewImageContainer: {
    backgroundColor: Theme.Colors.greyColor,
    height: height / 2,
    width: width,
    resizeMode: "contain",
  },
  buttonImage: {
    padding: 15,
    flexDirection: "row",
    width: scale(150),
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: Theme.Colors.greyColor,
  },
});

export default UpdateImageWorker;
