import React, { useEffect, useLayoutEffect, useState } from "react";
import { View, Alert, StyleSheet, ScrollView } from "react-native";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { useTranslation } from "react-i18next";
import { useForm } from "react-hook-form";
import { Button } from "react-native-paper";
import {
  ControlledTextInput,
  DateOfBirthInput,
  PickerSelect,
  ModalSpinner,
} from "@src/components";
import {
  useProvince,
  useCommune,
  useDistrict,
  useVillage,
} from "@src/hooks/useFilters";
import { updateWorker } from "@src/hooks/useWorker";
import { navigateToBack } from "@src/navigation/Navigation";
import { Theme } from "@src/config/theme";

const EditWorker: React.FC = ({ navigation, route }: any) => {
  const { t } = useTranslation();
  const { workers } = route.params;
  const queryClient = useQueryClient();
  const { handleSubmit, setValue, control, formState } = useForm({
    defaultValues: {
      ...workers,
    },
  });

  const [selectProvince, setSelectProvince] = useState<number>(0);
  const [selectCommune, setSelectCommune] = useState<number>(0);
  const [selectDistrict, setSelectDistrict] = useState<number>(0);
  const [selectVillage, setSelectVillage] = useState<number>(0);

  const { data: province, isLoading: loadingProvince } = useProvince();
  const { data: district, isLoading: loadingDistrict } =
    useDistrict(selectProvince);
  const { data: commune, isLoading: loadingCommune } =
    useCommune(selectDistrict);
  const { data: village, isLoading: loadingVillage } =
    useVillage(selectCommune);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: t("edit"),
      headerShown: true,
    });
  }, [navigation]);

  useEffect(() => {
    setSelectProvince(workers?.province_id);
    setSelectCommune(workers?.commune_id);
    setSelectDistrict(workers?.district_id);
    setSelectVillage(workers?.village_id);

    setValue("name_en", workers?.name_en);
    setValue("name_kh", workers?.name_kh);
    setValue("id_card", workers?.id_card);
    setValue("dob", workers?.dob);
    setValue("passport_no", workers?.passport_no);
    setValue("phone_number", workers?.phone_number);
  }, [workers]);

  const mutation = useMutation({
    mutationFn: updateWorker,
    onSuccess: (data: any) => {
      if (data) {
        queryClient.refetchQueries({ queryKey: ["workers-detail"] });
        navigateToBack(navigation);
      }
    },
    onError: (error, variables, context) => {
      Alert.alert("Failed edit worker");
    },
  });

  const onSubmit = (data: any) => {
    mutation.mutate(data);
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <ModalSpinner
        visible={
          loadingProvince || loadingCommune || loadingDistrict || loadingVillage
        }
      />

      <ControlledTextInput
        control={control}
        formState={formState}
        name="name_en"
        label={t("fullNameEn")}
        rules={{ required: t("required") }}
        style={styles.input}
      />
      <ControlledTextInput
        control={control}
        formState={formState}
        name="name_kh"
        label={t("fullNameKh")}
        rules={{ required: t("required") }}
        style={styles.input}
      />
      <ControlledTextInput
        control={control}
        formState={formState}
        name="id_card"
        label={t("idCard")}
        rules={{ required: t("required") }}
        style={styles.input}
      />
      <DateOfBirthInput
        control={control}
        formState={formState}
        name="dob"
        label={t("dob")}
        rules={{ required: t("required") }}
        style={styles.input}
      />
      <ControlledTextInput
        control={control}
        formState={formState}
        name="passport_no"
        label={t("passport")}
        rules={{ required: t("required") }}
        style={styles.input}
      />
      <ControlledTextInput
        control={control}
        formState={formState}
        name="phone_number"
        label={t("phoneNumber")}
        rules={{ required: t("required") }}
        style={styles.input}
      />
      <PickerSelect
        control={control}
        formState={formState}
        placeholder=""
        name="province_id"
        datas={province || []}
        label={t("province")}
        selectedId={selectProvince}
        handleChange={(id) => {
          setSelectProvince(id);
        }}
      />
      <PickerSelect
        control={control}
        formState={formState}
        placeholder=""
        name="district_id"
        datas={district || []}
        label={t("district")}
        selectedId={selectDistrict}
        handleChange={(id) => {
          setSelectDistrict(id);
        }}
      />
      <PickerSelect
        control={control}
        formState={formState}
        placeholder=""
        name="commune_id"
        datas={commune || []}
        label={t("communeDisPro")}
        selectedId={selectCommune}
        handleChange={(id) => {
          setSelectCommune(id);
        }}
      />
      <PickerSelect
        control={control}
        formState={formState}
        placeholder=""
        name="village_id"
        datas={village || []}
        label={t("village")}
        selectedId={selectVillage}
        handleChange={(id) => {
          setSelectVillage(id);
        }}
      />

      <View style={{ paddingTop: 30 }}>
        <Button
          mode="contained"
          loading={mutation.isPending}
          disabled={mutation.isPending}
          theme={{ colors: { primary: Theme.Colors.primary } }}
          contentStyle={{ height: 50 }}
          style={styles.button}
          onPress={handleSubmit(onSubmit)}
        >
          {t("save")}
        </Button>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
    paddingHorizontal: 20,
    flexGrow: 1,
    backgroundColor: Theme.Colors.lightBackground,
  },
  input: {
    marginBottom: 16,
  },
  button: {
    width: "100%",
  },
});

export default EditWorker;
