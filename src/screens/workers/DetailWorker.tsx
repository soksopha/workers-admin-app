import React, { useLayoutEffect } from "react";
import { ScrollView } from "react-native";
import { useTranslation } from "react-i18next";
import { ModalSpinner } from "@src/components";
import { WebView } from "react-native-webview";
import { WEB_VIEW } from "@env";

const DetailWorker = ({ navigation, route }: any) => {
  const { id } = route.params;
  const { t } = useTranslation();
  const [loading, setLoading] = React.useState(true);
  useLayoutEffect(() => {
    navigation.setOptions({
      title: t("viewDetail"),
      headerShown: true,
      contentStyle: {
        paddingTop: 0,
      },
    });
  }, [navigation]);

  return (
    <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
      <ModalSpinner visible={loading} />
      <WebView
        source={{ uri: `${WEB_VIEW}resume/${id}` }}
        style={{ flex: 1 }}
        onLoadEnd={() => {
          setLoading(false);
        }}
      />
    </ScrollView>
  );
};

export default DetailWorker;
