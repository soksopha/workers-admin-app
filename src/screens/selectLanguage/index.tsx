import React from "react";
import { useTranslation } from "react-i18next";
import {
  View,
  StyleSheet,
  StatusBar,
  TouchableOpacity,
  Image,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { Text } from "react-native-paper";
import { Theme } from "@src/config/theme";
import { AccessKey } from "@src/utils/enums";
import { resetNavigation } from "@src/navigation/Navigation";

interface LanguageProps {
  navigation: any;
}

const LanguageScreen: React.FC<LanguageProps> = ({ navigation }) => {
  const { t, i18n } = useTranslation();

  const handleLanguageSelect = (language: string) => {
    AsyncStorage.setItem(AccessKey.LANGUAGE, JSON.stringify(language));
    resetNavigation(navigation, "Login");
    i18n.changeLanguage(language);
  };

  return (
    <View style={styles.container}>
      <StatusBar
        animated={true}
        backgroundColor={Theme.Colors.silverColor}
        translucent={true}
        barStyle="dark-content"
      />
      <View style={styles.subContainer}>
        <View style={styles.inner}>
          <Image
            style={styles.logo}
            source={require("@src/assets/images/app_logo.png")}
            resizeMode="contain"
          />
          <Text style={styles.title} allowFontScaling={false}>
            {t("selectLanguage")}
          </Text>
        </View>
        <View
          style={[
            styles.inner,
            {
              marginTop: 75,
              flexDirection: "row",
              justifyContent: "space-around",
            },
          ]}
        >
          <TouchableOpacity
            onPress={() => handleLanguageSelect("km")}
            style={styles.touchContainer}
          >
            <Image
              style={styles.flag}
              source={require("@src/assets/images/flags/km.png")}
            />
            <Text style={styles.flagText} allowFontScaling={false}>
              ភាសាខ្មែរ
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => handleLanguageSelect("en")}
            style={styles.touchContainer}
          >
            <Image
              style={styles.flag}
              source={require("../../assets/images/flags/uk.png")}
            />
            <Text style={styles.flagText} allowFontScaling={false}>
              English
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  subContainer: {
    flex: 1,
    justifyContent: "center",
    padding: 15,
  },
  inner: {
    justifyContent: "center",
    alignItems: "center",
  },
  logo: {
    width: 120,
    height: 120,
  },
  title: {
    color: Theme.Colors.blackColor,
    fontSize: 25,
    marginTop: 10,
    marginBottom: 10,
    textAlign: "center",
  },
  subtitle: {
    marginTop: 0,
    fontSize: 16,
    textAlign: "center",
  },
  touchContainer: {
    width: 150,
    height: 150,
    borderRadius: 20,
    backgroundColor: Theme.Colors.whiteColor,
    borderColor: Theme.Colors.placeHolderColor,
    borderWidth: 0,
    justifyContent: "center",
    alignItems: "center",
  },
  flag: {
    height: 80,
    width: 80,
    marginBottom: 5,
  },
  flagText: {
    color: Theme.Colors.blackColor,
    fontSize: 16,
    marginTop: 0,
    textAlign: "center",
  },
  versionText: {
    fontSize: 12,
    textAlign: "center",
    color: "grey",
    marginBottom: 10,
  },
});

export default LanguageScreen;
