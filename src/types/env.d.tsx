declare module "@env" {
  export const API_DEV_HOST: string;
  export const API_HOST: string;
  export const WEB_VIEW: string;
  export const IMAGE_PATH: string;
  export const APP_VERSION: string;
}
