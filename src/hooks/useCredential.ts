import { baseApi } from "@src/services/baseApi";

const changeProfile = async (data: any) => {
  const response = await baseApi.post<any>("change/profile", data);
  return response?.data?.data;
};

const validateCredential = async (data: any) => {
  const response = await baseApi.post<any>("validate", data);
  return response?.data?.data;
};

const changePassword = async (data: any) => {
  const response = await baseApi.post<any>("change/password", data);
  return response?.data?.data;
};

export { changeProfile, changePassword, validateCredential };
