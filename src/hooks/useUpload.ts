import { baseApi } from "@src/services/baseApi";

const uploadImage = async (form: any) => {
  const response = await baseApi.postForm<any>("worker/image", form);
  return response?.data?.data;
};

const dropImage = async (form: any) => {
  const response = await baseApi.post<any>("worker/image/drop", form);
  return response?.data;
};

export { uploadImage, dropImage };
