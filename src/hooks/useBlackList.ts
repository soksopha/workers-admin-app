import { baseApi } from "@src/services/baseApi";

const blackList = async (data: any) => {
  try {
    const response = await baseApi.post<any>("worker/blacklist", data);
    return response?.data?.data;
  } catch (error) {
    throw error;
  }
};

const removeBlackList = async (data: any) => {
  try {
    const response = await baseApi.postForm<any>("worker/blacklist", data);
    return response?.data?.data;
  } catch (error) {
    throw error;
  }
};

const takeOut = async (data: any) => {
  try {
    const response = await baseApi.postForm<any>("worker/takeout", data);
    return response?.data?.data;
  } catch (error) {
    throw error;
  }
};

export { blackList, removeBlackList, takeOut };
