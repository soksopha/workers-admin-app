import { useQuery } from "@tanstack/react-query";
import AsyncStorage from "@react-native-community/async-storage";
import { baseApi } from "@src/services/baseApi";
import { useStorage } from "@src/hooks/useLocalStorage";
import { AccessKey } from "@src/utils/enums";

const twentyFourHoursInMs = 1000 * 60 * 60 * 24;

const listProvince = async () => {
  const response = await baseApi.get("province");
  return response?.data?.data;
};

const listDistrict = async (provinceId: any) => {
  const response = await baseApi.get(`district?province_id=${provinceId}`);
  return response?.data?.data;
};

const listCommune = async (districtId: any) => {
  const response = await baseApi.get(`commune?district_id=${districtId}`);
  return response?.data?.data;
};

const listVillage = async (communeId: any) => {
  const response = await baseApi.get(`village?commune_id=${communeId}`);
  return response?.data?.data;
};

const filterList = async () => {
  const response = await baseApi.get("filter");
  return response?.data?.data;
};

const factorList = async (countryId: number) => {
  const response = await baseApi.get(`factory?country_id=${countryId}`);
  return response?.data?.data;
};

const branchList = async (factoryId: number) => {
    const user: any = await AsyncStorage.getItem(AccessKey.PROFILE);
    const userData: any = JSON.parse(user);
    if (userData?.role_id == 4) {
      const factory: any = await AsyncStorage.getItem(AccessKey.FACTORY);
      const factoryData: any = JSON.parse(factory);
      return factoryData?.branches?.filter((data: any) => data.factory_id == factoryId);
    } else {
      const response = await baseApi.get(`branch?factory_id=${factoryId}`);
      return response?.data?.data;
    }
};

const useFilterList = () => {
  return useQuery({
    queryKey: ["filterList"],
    queryFn: () => filterList(),
  });
};

const useFactorList = (countryId: number) => {
  return useQuery({
    queryKey: ["factorList", countryId],
    queryFn: () => factorList(countryId),
    enabled: !!countryId,
  });
};

const useBranchList = (factoryId: number) => {
  return useQuery({
    queryKey: ["branchList", factoryId],
    queryFn: () => branchList(factoryId),
    enabled: !!factoryId,
  });
};

const useProvince = () => {
  return useQuery({
    queryKey: ["listProvince"],
    queryFn: () => listProvince(),
    staleTime: twentyFourHoursInMs,
  });
};

const useCommune = (districtId: any) => {
  return useQuery({
    queryKey: ["listCommune", districtId],
    queryFn: () => listCommune(districtId),
    enabled: !!districtId,
  });
};

const useVillage = (communeId: any) => {
  return useQuery({
    queryKey: ["listVillage", communeId],
    queryFn: () => listVillage(communeId),
    enabled: !!communeId,
  });
};

const useDistrict = (provinceId: any) => {
  return useQuery({
    queryKey: ["listDistrict", provinceId],
    queryFn: () => listDistrict(provinceId),
    enabled: !!provinceId,
  });
};

export {
  useFilterList,
  useFactorList,
  useBranchList,
  useProvince,
  useCommune,
  useVillage,
  useDistrict,
};
