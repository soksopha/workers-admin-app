import { useQuery, useInfiniteQuery } from "@tanstack/react-query";
import { baseApi } from "@src/services/baseApi";

const updateWorker = async (data: any) => {
  const response = await baseApi.post("worker/update", data);
  return response?.data?.data;
};

const fetchWorker = async (params: any) => {
  const {
    search = "",
    agency_id = "",
    country_id = "",
    factory_id = "",
    branch_id = "",
    status = "",
    process_status = "",
    page,
  } = params;

  const searchParams = new URLSearchParams({
    search,
    agency_id,
    country_id,
    factory_id,
    branch_id,
    status,
    process_status,
    page,
  });
  const queryString = searchParams.toString();
  const response = await baseApi.get(`worker?${queryString}`);
  return response?.data?.data;
};

const fetchBlackList = async () => {
  const response = await baseApi.get("worker");
  return response?.data?.data;
};

const fetchWorkerDetail = async (id: number) => {
  const response = await baseApi.get(`worker/${id}`);
  return response?.data?.data;
};

const useSearchWorker = (search: any) => {
  return useQuery({
    queryKey: ["list-workers", search],
    queryFn: () => fetchWorker(search),
  });
};

const useFetchWorker = (search: any, enabled?: boolean) => {
  return useInfiniteQuery({
    queryKey: ["search-workers", search],
    queryFn: ({ pageParam = 1 }) =>
      fetchWorker({
        ...search,
        page: pageParam,
      }),
    initialPageParam: 1,
    enabled: enabled,
    getNextPageParam: (lastPage) => {
      return Number(lastPage.page) < Number(lastPage.total_page)
        ? Number(lastPage.page) + 1
        : undefined;
    },
  });
};

const useFetchWorkerBlackList = () => {
  return useQuery({
    queryKey: ["list-workers-blacklists"],
    queryFn: () => fetchBlackList(),
  });
};

const useFetchWorkerDetail = (id: number) => {
  return useQuery({
    queryKey: ["workers-detail", id],
    queryFn: () => fetchWorkerDetail(id),
    enabled: !!id,
  });
};

export {
  useFetchWorker,
  useFetchWorkerDetail,
  useFetchWorkerBlackList,
  useSearchWorker,
  updateWorker,
};
