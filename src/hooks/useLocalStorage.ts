import { useQuery } from "@tanstack/react-query";
import AsyncStorage from "@react-native-community/async-storage";

const fetchStorage = async (key: string) => {
  const data = await AsyncStorage.getItem(key);
  const response = data ? JSON.parse(data) : null;
  return response;
};

const useStorage = (key: string) => {
  return useQuery({
    queryKey: ["user-storage", key],
    queryFn: () => fetchStorage(key),
  });
};

export { useStorage };
