import { useQuery } from "@tanstack/react-query";
import { baseApi } from "@src/services/baseApi";

const fetchDashboard = async (params: any) => {
  let { country = "", factory = "", branch = "" } = params;

  country = country === 0 ? "" : country;
  factory = factory === 0 ? "" : factory;
  branch = branch === 0 ? "" : branch;

  const searchParams = new URLSearchParams({
    country,
    factory,
    branch,
  });

  const queryString = searchParams.toString();
  const response = await baseApi.get(`dashboard?${queryString}`);


  console.log(JSON.stringify(response?.data?.data));
  return response?.data?.data;
};

const useFetchDashboard = (search: any) => {
  return useQuery({
    queryKey: ["dashboard", search],
    queryFn: () => fetchDashboard(search),
  });
};

export { useFetchDashboard };
