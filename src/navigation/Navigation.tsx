import { NavigationProp, StackActions } from "@react-navigation/native";

export const navigateTo = (
  navigation: NavigationProp<any>,
  route: string,
  params = {},
) => {
  navigation.navigate(route, params);
};

export const navigateToBack = (navigation: NavigationProp<any>) => {
  navigation.goBack();
};

export const resetNavigation = (
  navigation: NavigationProp<any>,
  targetRoute: string,
  params?: any,
) => {
  navigation.dispatch(StackActions.replace(targetRoute, params));
};
