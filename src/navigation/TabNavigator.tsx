import React from "react";
import Icon from "react-native-vector-icons/Feather";
import { useTranslation } from "react-i18next";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Platform } from "react-native";
import Home from "@src/screens/home";
import Search from "@src/screens/search";
import Setting from "@src/screens/setting";
import { Theme } from "@src/config/theme";

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  const { t } = useTranslation();

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarHideOnKeyboard: true,
        tabBarStyle: {
          height: Platform.OS === "ios" ? 90 : 70,
          paddingTop: 10,
          paddingBottom: Platform.OS === "ios" ? 30 : 10,
          backgroundColor: Theme.Colors.whiteColor,
        },
        tabBarLabelStyle: { fontFamily: "Kantumruy-Regular" },
        tabBarActiveTintColor: Theme.Colors.primary,
        tabBarInactiveTintColor: Theme.Colors.blackColor,
        headerShown: false,
      }}
    >
      <Tab.Screen
        name="home"
        component={Home}
        options={{
          tabBarLabel: t("tabHome"),
          tabBarIcon: ({ color, size }) => (
            <Icon name="home" color={color} size={size} />
          ),
        }}
      />
      <Tab.Screen
        name="search"
        component={Search}
        options={({ route }) => ({
          tabBarLabel: t("tabSearch"),
          tabBarIcon: ({ color, size }) => (
            <Icon name="search" color={color} size={size} />
          ),
        })}
      />
      <Tab.Screen
        name="setting"
        component={Setting}
        options={{
          tabBarLabel: t("tabSetting"),
          tabBarIcon: ({ color, size }) => (
            <Icon name="settings" color={color} size={size} />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigator;
