import React from "react";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import LoginScreen from "@src/screens/auth/Login";
import AuthLoading from "@src/screens/auth/AuthLoading";
import SelectLanguageScreen from "@src/screens/selectLanguage";
import ProfileScreen from "@src/screens/setting/Profile";
import ChangeProfileScreen from "@src/screens/setting/ChangeProfile";
import ChangePasswordScreen from "@src/screens/setting/ChangePassword";
import HistoryWorkerScreen from "@src/screens/workers/HistoryWorker";
import UpdateImageWorkerScreen from "@src/screens/workers/UpdateImageWorker";
import EditWorkerScreen from "@src/screens/workers/EditWorker";
import DetailWorkerScreen from "@src/screens/workers/DetailWorker";
import EditBlackListScreen from "@src/screens/workers/EditBlackList";
import FilterWorkerScreen from "@src/screens/home/filterWorker";
import App from "@src/navigation/TabNavigator";
import { Theme } from "@src/config/theme";

const Stack = createNativeStackNavigator();
const AuthStack = () => {
    const insets = useSafeAreaInsets();

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                headerTitleAlign: "center",
                headerStyle: {
                    backgroundColor: Theme.Colors.lightBackground,
                },
                headerTintColor: Theme.Colors.blackColor,
                headerTitleStyle: {
                    fontSize: 15,
                    fontFamily: Theme.Fonts.khBold,
                },
                headerBackTitleVisible: false,
                contentStyle: {
                    paddingTop: insets.top,
                    backgroundColor: Theme.Colors.lightBackground,
                },
            }}
            initialRouteName="AuthLoading"
        >
            <Stack.Screen name="AuthLoading" component={AuthLoading} />
            <Stack.Screen
                name="SelectLanguage"
                component={SelectLanguageScreen}
            />
            <Stack.Screen name="Login" component={LoginScreen} />
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    contentStyle: {
                        paddingTop: 0,
                    },
                }}
            />
            <Stack.Screen
                name="ChangeProfile"
                component={ChangeProfileScreen}
                options={{
                    contentStyle: {
                        paddingTop: 0,
                    },
                }}
            />
            <Stack.Screen
                name="ChangePassword"
                component={ChangePasswordScreen}
                options={{
                    contentStyle: {
                        paddingTop: 0,
                    },
                }}
            />
            <Stack.Screen
                name="HistoryWorker"
                component={HistoryWorkerScreen}
                options={{
                    contentStyle: {
                        paddingTop: 0,
                    },
                }}
            />
            <Stack.Screen
                name="UploadImageWorker"
                component={UpdateImageWorkerScreen}
            />
            <Stack.Screen name="EditWorker" component={EditWorkerScreen} />
            <Stack.Screen name="DetailWorker" component={DetailWorkerScreen} />
            <Stack.Screen
                name="EditBlackList"
                component={EditBlackListScreen}
            />
            <Stack.Screen
                name="FilterWorker"
                component={FilterWorkerScreen}
                options={{
                    headerStyle: {
                        backgroundColor: Theme.Colors.lightBackground,
                    },
                    contentStyle: {
                        paddingTop: 0,
                    },
                }}
            />
            <Stack.Screen name="App" component={App} />
        </Stack.Navigator>
    );
};

export default AuthStack;
