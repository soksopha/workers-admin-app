import axios from "axios";
import { Alert } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { AccessKey } from "@src/utils/enums";
import { API_HOST } from "@env";

const apiBaseUrl = API_HOST;
const setAuthToken = async (config: any) => {
  try {
    const token = await AsyncStorage.getItem(AccessKey.TOKEN);
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  } catch (error) {
    console.error("Error setting authorization token:", error);
    return Promise.reject(error);
  }
};

const handleServerError = (error: any) => {
  if (error.response) {
    const status = error.response.status;
    if (status === 401 || status === 403) {
      return new Promise((resolve, reject) => {
        Alert.alert(
          "Unauthorized Access",
          "The token is incorrect. Please Re-login.",
          [
            {
              text: "OK",
              onPress: () => {
                reject(error);
              },
            },
          ],
        );
      });
    }
  } else if (error.request) {
    Alert.alert("Server Error", "Something when wrong !", [
      {
        text: "OK",
        onPress: () => console.log("OK Pressed"),
      },
    ]);
  } else {
    console.log("Request error:", error.message);
  }

  return Promise.reject(error);
};

export const baseApi = axios.create({
  baseURL: apiBaseUrl,
  withCredentials: true,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json",
  },
});

baseApi.interceptors.request.use(setAuthToken);
baseApi.interceptors.response.use((response) => response, handleServerError);
