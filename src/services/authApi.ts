import { baseApi } from "./baseApi";

export const login = async (user: any) => {
  const response = await baseApi.post<any>("login", user);
  return response?.data?.data;
};
