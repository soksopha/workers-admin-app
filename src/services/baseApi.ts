import axios from "axios";
import { useNavigation } from "@react-navigation/native";
import { Alert } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { AccessKey } from "@src/utils/enums";
import { API_HOST } from "@env";

const setAuthToken = async (config: any) => {
  try {
    const token = await AsyncStorage.getItem(AccessKey.TOKEN);
    if (token) {
      config.headers["Authorization"] = `Bearer ${token}`;
    }
    return config;
  } catch (error) {
    console.error("Error setting authorization token:", error);
    return Promise.reject(error);
  }
};

const handleServerError = (error: any) => {
  if (error.response) {
    const navigation: any = useNavigation();
    const status = error.response.status;
    if (status === 401 || status === 403) {
      return new Promise((resolve, reject) => {
        Alert.alert(
          "Unauthorized Access",
          "The token is incorrect. Please Re-login.",
          [
            {
              text: "OK",
              onPress: () => {
                navigation.navigate("Login");
                reject(error);
              },
            },
          ],
        );
      });
    }
  } else {
    console.log("Request error:", error.message);
  }

  return Promise.reject(error);
};

export const baseApi = axios.create({
  baseURL: API_HOST,
  withCredentials: true,
  headers: {
    Accept: "application/json",
    Connection: "close",
    "Content-Type": "application/json",
  },
});

baseApi.interceptors.request.use(setAuthToken);
baseApi.interceptors.response.use((response) => response, handleServerError);
