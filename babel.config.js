module.exports = {
  presets: [
    'module:metro-react-native-babel-preset',
    '@babel/preset-typescript',
  ],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        alias: {
          '@src': './src',
          '@/': './src',
        },
      },
    ],
    'react-native-reanimated/plugin', //Add this line for absolute path (@src)
    [
      'module:react-native-dotenv',
      {
        //Add this line for absolute path (@env)
        moduleName: '@env',
        path: '.env',
        safe: false,
        allowUndefined: false,
      },
    ],
  ],
};
